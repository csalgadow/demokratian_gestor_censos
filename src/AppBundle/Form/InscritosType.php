<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
//use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use AppBundle\Form\Listener\AddStateFieldSubscriber;

class InscritosType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nombreUsuario', TextType::class, array("label" => "Nombre: ",
                   "required" => "required",
                    "attr" => array("class" => "form-control input")))
                ->add('apellidoUsuario', TextType::class, array("label" => "Apellidos: ",
                    "required" => "required",
                    "attr" => array("class" => "form-name form-control input")))
                ->add('telefono', TextType::class, array("label" => "Telefono: ",
                    "required" => "required",
                    "attr" => array("class" => "form-name form-control")))
//                ->add('correoUsuario', EmailType::class, array("label" => "Correo: ",
//                    "required" => "required",
//                    "attr" => array("class" => "form-email form-control")))
                ->add('correoUsuario', RepeatedType::class, array(
                    "required" => "required",
                    'type' => EmailType::class,
                    'invalid_message' => 'Los dos correos deben coincidir',
                    'first_options' => array('label' => 'Correo electronico', "attr" => array("class" => "form-email form-control")),
                    'second_options' => array('label' => 'Repita el Correo electronico', "attr" => array("class" => "form-email form-control"))
                ))
                ->add('tipodocumento', ChoiceType::class, array("label" => "Tipo de documento: ",
                    "choices" => array(
                        "DNI" => "DNI",
                        "NIE" => "NIE",
                        "Pasaporte" => "pasaporte"),
                    "attr" => array("class" => "form-name form-control")))
                ->add('documento', TextType::class, array("label" => "Documento: ",
                    "required" => "required",
                    "attr" => array("class" => "form-name form-control")))
                ->add('fechanacimiento', DateType::class, array("label" => "Fecha de nacimiento: ",
                    "required" => "required",
                    'widget' => 'single_text',
                    // this is actually the default format for single_text
                    'format' => 'yyyy-MM-dd',
                    //////////////////////////
                    //'widget' => 'single_text',
                    // do not render as type="date", to avoid HTML5 date pickers
                    //    'html5' => false,
                    // add a class that can be selected in JavaScript
                    //    'attr' => ['class' => 'js-datepicker'],
                    "attr" => array("class" => "form-dateform-control")))
                ->add('pais', ChoiceType::class, array("label" => "Pais: ",
                    "choices" => array(
                        "Afganistán" => "AF",
                        "Åland" => "AX",
                        "Albania" => "AL",
                        "Alemania" => "DE",
                        "Andorra" => "AD",
                        "Angola" => "AO",
                        "Anguila" => "AI",
                        "Antártida" => "AQ",
                        "Antigua y Barbuda" => "AG",
                        "Arabia Saudita" => "SA",
                        "Argelia" => "DZ",
                        "Argentina" => "AR",
                        "Armenia" => "AM",
                        "Aruba" => "AW",
                        "Australia" => "AU",
                        "Austria" => "AT",
                        "Autoridad Nacional Palestina" => "PS",
                        "Azerbaiyán" => "AZ",
                        "Bahamas" => "BS",
                        "Bangladés" => "BD",
                        "Barbados" => "BB",
                        "Baréin" => "BH",
                        "Bélgica" => "BE",
                        "Belice" => "BZ",
                        "Benín" => "BJ",
                        "Bermudas" => "BM",
                        "Bielorrusia" => "BY",
                        "Birmania" => "MM",
                        "Bolivia" => "BO",
                        "Bonaire, San Eustaquio y Saba" => "BQ",
                        "Bosnia y Herzegovina" => "BA",
                        "Botsuana" => "BW",
                        "Brasil" => "BR",
                        "Brunéi" => "BN",
                        "Bulgaria" => "BG",
                        "Burkina Faso " => "BF",
                        "Burundi" => "BI",
                        "Bután" => "BT",
                        "Cabo Verde" => "CV",
                        "Camboya" => "KH",
                        "Camerún" => "CM",
                        "Canadá" => "CA",
                        "Chad" => "TD",
                        "Chile" => "CL",
                        "China" => "CN",
                        "Chipre" => "CY",
                        "Ciudad del Vaticano" => "VA",
                        "Colombia" => "CO",
                        "Comoras" => "KM",
                        "Corea del Norte" => "KP",
                        "Corea del Sur" => "KR",
                        "Costa de Marfil" => "CI",
                        "Costa Rica" => "CR",
                        "Croacia" => "HR",
                        "Cuba" => "CU",
                        "Curaçao" => "CW",
                        "Dinamarca" => "DK",
                        "Dominica" => "DM",
                        "Ecuador" => "EC",
                        "Egipto" => "EG",
                        "El Salvador" => "SV",
                        "Emiratos Árabes Unidos" => "AE",
                        "Eritrea" => "ER",
                        "Eslovaquia" => "SK",
                        "Eslovenia" => "SI",
                        "España" => "ES",
                        "Estados Unidos" => "US",
                        "Estonia" => "EE",
                        "Etiopía" => "ET",
                        "Filipinas" => "PH",
                        "Finlandia" => "FI",
                        "Fiyi" => "FJ",
                        "Francia" => "FR",
                        "Gabón" => "GA",
                        "Gambia" => "GM",
                        "Georgia" => "GE",
                        "Ghana" => "GH",
                        "Gibraltar" => "GI",
                        "Granada" => "GD",
                        "Grecia" => "GR",
                        "Groenlandia" => "GL",
                        "Guadalupe" => "GP",
                        "Guam" => "GU",
                        "Guatemala" => "GT",
                        "Guayana Francesa" => "GF",
                        "Guernsey" => "GG",
                        "Guinea" => "GN",
                        "Guinea Ecuatorial" => "GQ",
                        "Guinea-Bissau" => "GW",
                        "Guyana" => "GY",
                        "Haití" => "HT",
                        "Honduras" => "HN",
                        "Hong Kong" => "HK",
                        "Hungría" => "HU",
                        "India" => "IN",
                        "Indonesia" => "ID",
                        "Irak" => "IQ",
                        "Irán" => "IR",
                        "Irlanda" => "IE",
                        "Isla Bouvet" => "BV",
                        "Isla de Man" => "IM",
                        "Isla de Navidad" => "CX",
                        "Islandia" => "IS",
                        "Islas Caimán" => "KY",
                        "Islas Cocos" => "CC",
                        "Islas Cook" => "CK",
                        "Islas Faroe" => "FO",
                        "Islas Georgias del Sur y Sandwich del Sur" => "GS",
                        "Islas Heard y McDonald" => "HM",
                        "Islas Malvinas" => "FK",
                        "Islas Marianas del Norte" => "MP",
                        "Islas Marshall" => "MH",
                        "Islas Pitcairn" => "PN",
                        "Islas Salomón" => "SB",
                        "Islas Turcas y Caicos" => "TC",
                        "Islas ultramarinas de Estados Unidos" => "UM",
                        "Islas Vírgenes Británicas" => "VG",
                        "Islas Vírgenes de los Estados Unidos" => "VI",
                        "Israel" => "IL",
                        "Italia" => "IT",
                        "Jamaica" => "JM",
                        "Japón" => "JP",
                        "Jersey" => "JE",
                        "Jordania" => "JO",
                        "Kazajistán" => "KZ",
                        "Kenia" => "KE",
                        "Kirguistán" => "KG",
                        "Kiribati" => "KI",
                        "Kuwait" => "KW",
                        "Laos" => "LA",
                        "Lesoto" => "LS",
                        "Letonia" => "LV",
                        "Líbano" => "LB",
                        "Liberia" => "LR",
                        "Libia" => "LY",
                        "Liechtenstein" => "LI",
                        "Lituania" => "LT",
                        "Luxemburgo" => "LU",
                        "Macao" => "MO",
                        "Madagascar" => "MG",
                        "Malasia" => "MY",
                        "Malaui" => "MW",
                        "Maldivas" => "MV",
                        "Malí" => "ML",
                        "Malta" => "MT",
                        "Marruecos" => "MA",
                        "Martinica" => "MQ",
                        "Mauricio" => "MU",
                        "Mauritania" => "MR",
                        "Mayotte" => "YT",
                        "México" => "MX",
                        "Micronesia" => "FM",
                        "Moldavia" => "MD",
                        "Mónaco" => "MC",
                        "Mongolia" => "MN",
                        "Montenegro" => "ME",
                        "Montserrat" => "MS",
                        "Mozambique" => "MZ",
                        "Namibia" => "NA",
                        "Nauru" => "NR",
                        "Nepal" => "NP",
                        "Nicaragua" => "NI",
                        "Níger" => "NE",
                        "Nigeria" => "NG",
                        "Niue" => "NU",
                        "Norfolk" => "NF",
                        "Noruega" => "NO",
                        "Nueva Caledonia" => "NC",
                        "Nueva Zelanda" => "NZ",
                        "Omán" => "OM",
                        "Países Bajos" => "NL",
                        "Pakistán" => "PK",
                        "Palaos" => "PW",
                        "Panamá" => "PA",
                        "Papúa Nueva Guinea" => "PG",
                        "Paraguay" => "PY",
                        "Perú" => "PE",
                        "Polinesia Francesa" => "PF",
                        "Polonia" => "PL",
                        "Portugal" => "PT",
                        "Puerto Rico" => "PR",
                        "Qatar" => "QA",
                        "Reino Unido" => "GB",
                        "Rep. Dem. del Congo" => "CD",
                        "República Centroafricana" => "CF",
                        "República Checa" => "CZ",
                        "República de Macedonia" => "MK",
                        "República del Congo" => "CG",
                        "República Dominicana" => "DO",
                        "Reunión" => "RE",
                        "Ruanda" => "RW",
                        "Rumania" => "RO",
                        "Rusia" => "RU",
                        "Sáhara Occidental" => "EH",
                        "Samoa" => "WS",
                        "Samoa Estadounidense" => "AS",
                        "San Bartolomé" => "BL",
                        "San Cristóbal y Nieves" => "KN",
                        "San Marino" => "SM",
                        "San Martín" => "MF",
                        "San Martín (parte holandesa)" => "SX",
                        "San Pedro y Miquelón" => "PM",
                        "San Vicente y las Granadinas" => "VC",
                        "Santa Helena, A. y T." => "SH",
                        "Santa Lucía" => "LC",
                        "Santo Tomé y Príncipe" => "ST",
                        "Senegal" => "SN",
                        "Serbia" => "RS",
                        "Seychelles" => "SC",
                        "Sierra Leona" => "SL",
                        "Singapur" => "SG",
                        "Siria" => "SY",
                        "Somalia" => "SO",
                        "Sri Lanka" => "LK",
                        "Suazilandia" => "SZ",
                        "Sudáfrica" => "ZA",
                        "Sudán" => "SD",
                        "Sudán del Sur" => "SS",
                        "Suecia" => "SE",
                        "Suiza" => "CH",
                        "Surinam" => "SR",
                        "Svalbard y Jan Mayen" => "SJ",
                        "Tailandia" => "TH",
                        "Taiwán" => "TW",
                        "Tanzania" => "TZ",
                        "Tayikistán" => "TJ",
                        "Territorio Británico del Océano Índico" => "IO",
                        "Territorios Australes Franceses" => "TF",
                        "Timor Oriental" => "TL",
                        "Togo" => "TG",
                        "Tokelau" => "TK",
                        "Tonga" => "TO",
                        "Trinidad y Tobago" => "TT",
                        "Túnez" => "TN",
                        "Turkmenistán" => "TM",
                        "Turquía" => "TR",
                        "Tuvalu" => "TV",
                        "Ucrania" => "UA",
                        "Uganda" => "UG",
                        "Uruguay" => "UY",
                        "Uzbekistán" => "UZ",
                        "Vanuatu" => "VU",
                        "Venezuela" => "VE",
                        "Vietnam" => "VN",
                        "Wallis y Futuna" => "WF",
                        "Yemen" => "YE",
                        "Yibuti" => "DJ",
                        "Zambia" => "ZM",
                        "Zimbabue" => "ZW"
                    ),
                    'data' => 'ES',
                    "attr" => array("class" => "form-name form-control")))
                ->add('provinciaotros', TextType::class, array("label" => "Provincia/Estado: ",
                    'required'   => false,
                    "attr" => array("class" => "form-name form-control")))
                ->add('municipiootros', TextType::class, array("label" => "Municipios : ",
                    'required'   => false,
                    "attr" => array("class" => "form-name form-control")))
                ->add('codigopostal', TextType::class, array("label" => "Codigo postal: ",
                    "required" => "required",
                    "attr" => array("class" => "form-name form-control")))
                ->add('direccion', TextType::class, array("label" => "Direccion: ",
                    "required" => "required",
                    "attr" => array("class" => "form-name form-control")))
                ->add('lugarparticipacion', TextType::class, array("label" => "Lugar participación: ",
                    "required" => "required",
                    "attr" => array("class" => "form-name form-control")))
//                ->add('ccaaInscritos', EntityType::class, array("label" => "CCAA: ",
//                    "class" => "BackendBundle:Ccaa",
//                    "attr" => array("class" => "form-name form-control")))
                ->add('provinciaInscritos', EntityType::class, array("label" => "Provincia: ",
                    'required'   => false,
                    "class" => "BackendBundle:Provincia",
                    'placeholder' => 'Selecciona una provincia',
                    "attr" => array("class" => "form-name form-control class_select_provincia")))
                ->add('municipioInscritos', EntityType::class, array("label" => "Municipio: ",
                    'required'   => false,
                    "class" => "BackendBundle:Municipios",
                    'placeholder' => 'Primero selecciona una provincia',
                    "attr" => array("class" => "form-name form-control  class_select_municipio")))
                ->add("acepto", CheckboxType::class, array(
                    "label" => "He leído y acepto las condiciones legales: ",
                    "required" => "required",
                    "mapped" => false,
                    "attr" => array('class' => "form-control css-checkbox" ),
                        )
                )
        ;
        // Añadimos un EventListener que actualizará el campo state
        // para que sus opciones correspondan
        // con el pais seleccionado por el usuario
        // $builder->addEventSubscriber(new AddStateFieldSubscriber());
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'BackendBundle\Entity\Inscritos'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'backendbundle_inscritos';
    }

}
