<?php

namespace BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class InscritosAccionesType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                // ->add('status')
                ->add('accion', TextareaType::class, array("label" => "Información de la acción: ",
                    "required" => "required",
                    "attr" => array("class" => "form-name form-control col-sm-4 col-md-3")))
                ->add('datos', TextareaType::class, array("label" => "Datos complementarios: ",
                    "required" => "required",
                    "attr" => array("class" => "form-name form-control col-sm-4 col-md-3")))

        ;

        if ('SUPER_ADMIN' === $options['accion']) {
            $builder
                    ->add('status', EntityType::class, array("label" => "Acción: ",
                        'required' => false,
                        "class" => "BackendBundle:Acciones",
                        'query_builder' => function (EntityRepository $er) {
                            return $er->createQueryBuilder('u')
                                    //     ->where("u.id != 2")
                                    ->orderBy('u.name', 'ASC');
                        },
                        'placeholder' => 'Selecciona una acción',
                        "attr" => array("class" => "form-name form-control col-sm-4 col-md-3")));
        } elseif ('EDIT' === $options['accion']) {
            // cuando se edita no se permite el cambio
        } else {
            $builder
                    ->add('status', EntityType::class, array("label" => "Acción: ",
                        'required' => false,
                        "class" => "BackendBundle:Acciones",
                        'query_builder' => function (EntityRepository $er) {
                            return $er->createQueryBuilder('u')
                                    ->where("u.id != 2")
                                    ->orderBy('u.name', 'ASC');
                        },
                        'placeholder' => 'Selecciona una acción',
                        "attr" => array("class" => "form-name form-control col-sm-4 col-md-3")));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'BackendBundle\Entity\InscritosAcciones',
            'accion' => 'ADMIN',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'backendbundle_inscritosacciones';
    }

}
