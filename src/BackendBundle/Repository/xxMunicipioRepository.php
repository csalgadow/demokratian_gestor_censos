<?php
namespace BackendBundle\Repository;

use BackendBundle\Entity\Municipios;
use Doctrine\ORM\EntityRepository;

class MunicipioRepository extends EntityRepository{

public function findByIdProvincia($provincia_id){

    $query = $this->getEntityManager()->createQuery("
        SELECT nombre
        FROM BackendBundle:Municipios nombre
        LEFT JOIN nombre.idProvincia provin
        WHERE provin.id = :provincia_id
    ")->setParameter('provincia_id', $provincia_id);

    return $query->getArrayResult();
} 
}