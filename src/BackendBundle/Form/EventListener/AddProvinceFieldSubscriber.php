<?php
//AddProvinceFieldSubscriber
namespace BackendBundle\Form\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use BackendBundle\Entity\Provincia;
use Doctrine\ORM\EntityRepository;

class AddProvinceFieldSubscriber implements EventSubscriberInterface {

    private $propertyPathToMunicipio;

    public function __construct($propertyPathToMunicipio) {
        $this->propertyPathToMunicipio = $propertyPathToMunicipio;
    }

    public static function getSubscribedEvents() {
        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT => 'preSubmit'
        );
    }

    private function addProvinceForm($form, $Province = null) {
        $formOptions = array(
            'class' => 'BackendBundle:Provincia',
            'mapped' => false,
            'required' => 'required',
            'label' => 'Provincia',
            'attr' => array(
                'class' => 'form-name form-control class_select_provincia',
            ),
        );

        if ($Province) {
            $formOptions['data'] = $Province;
        }

        $form->add('provinciaInscritos', EntityType::class, $formOptions);
    }

    public function preSetData(FormEvent $event) {
        $data = $event->getData();
        $form = $event->getForm();

        if (null === $data) {
            return;
        }

        $accessor = PropertyAccess::createPropertyAccessor();

        $municipio = $accessor->getValue($data, $this->propertyPathToMunicipio);
        $provincia = ($municipio) ? $municipio->getIdMunicipio()->getProvincia() : null;

        $this->addProvinceForm($form, $provincia);
    }

    public function preSubmit(FormEvent $event) {
        $form = $event->getForm();

        $this->addProvinceForm($form);
    }

}
