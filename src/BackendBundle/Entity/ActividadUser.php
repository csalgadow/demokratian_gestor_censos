<?php

namespace BackendBundle\Entity;

/**
 * ActividadUser
 */
class ActividadUser
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $actividadId;

    /**
     * @var string
     */
    private $actividad;

    /**
     * @var \BackendBundle\Entity\Users
     */
    private $users;

    /**
     * @var \BackendBundle\Entity\Actividad
     */
    private $actividades;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set actividadId
     *
     * @param integer $actividadId
     *
     * @return ActividadUser
     */
    public function setActividadId($actividadId)
    {
        $this->actividadId = $actividadId;

        return $this;
    }

    /**
     * Get actividadId
     *
     * @return integer
     */
    public function getActividadId()
    {
        return $this->actividadId;
    }

    /**
     * Set actividad
     *
     * @param string $actividad
     *
     * @return ActividadUser
     */
    public function setActividad($actividad)
    {
        $this->actividad = $actividad;

        return $this;
    }

    /**
     * Get actividad
     *
     * @return string
     */
    public function getActividad()
    {
        return $this->actividad;
    }

    /**
     * Set users
     *
     * @param \BackendBundle\Entity\Users $users
     *
     * @return ActividadUser
     */
    public function setUsers(\BackendBundle\Entity\Users $users = null)
    {
        $this->users = $users;

        return $this;
    }

    /**
     * Get users
     *
     * @return \BackendBundle\Entity\Users
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set actividades
     *
     * @param \BackendBundle\Entity\Actividad $actividades
     *
     * @return ActividadUser
     */
    public function setActividades(\BackendBundle\Entity\Actividad $actividades = null)
    {
        $this->actividades = $actividades;

        return $this;
    }

    /**
     * Get actividades
     *
     * @return \BackendBundle\Entity\Actividad
     */
    public function getActividades()
    {
        return $this->actividades;
    }
    /**
     * @var \BackendBundle\Entity\Actividad
     */
    private $laactividad;


    /**
     * Set laactividad
     *
     * @param \BackendBundle\Entity\Actividad $laactividad
     *
     * @return ActividadUser
     */
    public function setLaactividad(\BackendBundle\Entity\Actividad $laactividad = null)
    {
        $this->laactividad = $laactividad;

        return $this;
    }

    /**
     * Get laactividad
     *
     * @return \BackendBundle\Entity\Actividad
     */
    public function getLaactividad()
    {
        return $this->laactividad;
    }
}
