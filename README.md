# README #

##Donar##

Si quieres ayudarnos a mantener el proyecto, puedes hacer una donación.
[DONAR con PAYPAL](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=LAC3ZCRAWYAU2&lc=ES&item_name=Donaci%c3%b3n%20a%20%20Demokratian%2eorg&currency_code=EUR&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted)
También puedes donar tu tiempo y participar en el desarrollo de DEMOKRATIAN, contacta con nosotros info@demokratian.org

### ¿Para que es este repositorio? ###

* DEMOKRATIAN GESTOR DE CENSOS, una plataforma desarrollada por [Carlos Salgado Werner](http://carlos-salgado.es) que permite que los usuarios puedan inscribirse mediante un formulario , una vez ha completado dicho formulario tendrán que validar su correo y posteriormente adjuntar la documentación requerida.
Al día de hoy solo esta operativo el envío de fotos de la documentación que se solicite aunque se prevé ir añadiendo otras formas de validación como DNI electrónico, certificados CRT o grabación de video.
Una vez enviada la documentación, el numero de validadores que determine la organización deberá comprobar la documentación antes de que el inscrito este definitivamente registrado
* VERSION  Alfa 0.0
* Puedes descargar la ultima versión en: (https://bitbucket.org/csalgadow/demokratian_gestor_censos/)
* Tienes información adicional en el [WIKI del repositorio](https://bitbucket.org/csalgadow/demokratian_gestor_censos/admin/wiki/)
* Web de [demokratian.org](http://demokratian.org)

### Instalando la aplicación ###
#### MUY IMPORTANTE ####
** Recuerda que la base de este software es symfony y que por tanto, deberas dirigir la url de tu dominio a la carpeta web **

* Tienes mas inoformación sobre la instalación en [instrucciones de Instalacón](https://bitbucket.org/csalgadow/demokratian_gestor_censos/wiki/Instalar%20DEMOKRATIAN%20GESTOR%20DE%20CENSOS)

#### Antes de empezar####

La aplicación **Demopkratian gestor de censos** usa el framework de synfony  por lo que deberás instalar todas las dependencias de symfony .
La forma más fácil de hacerlo es instalando primero  COMPOSER  [¿que es composer?](https://bitbucket.org/equipo_demokratian/demokratian_gestor_censos/wiki/que%20es%20composer)  


En primer lugar debes tener Composer instalador globalmente. Si utilizas Linux o Mac OS X, ejecuta los siguientes comandos:

```
$ git clone https://bitbucket.org/equipo_demokratian/demokratian_gestor_censos.git/wiki
```

Si utilizas Windows, descárgate el [instalador ejecutable de Composer](https://getcomposer.org/download/) y sigue los pasos indicados por el instalador.



```
# clona el código de la aplicación
$ cd htdocs/
$ git clone git@bitbucket.org:equipo_demokratian/demokratian_gestor_censos.git

# instala las dependencias del proyecto (incluyendo Symfony)
$ cd demokratian_gestor_censos.git/
$ composer install

```

#### Instalar la aplicación ####

Una vez instalado Composer, ejecuta los siguientes comandos para descargar e instalar la aplicación DEMOKRATIAN GESTOR DE CENSOS

```
# clona el código de la aplicación
$ cd htdocs/
$ git clone git@bitbucket.org:equipo_demokratian/demokratian_gestor_censos.git

# instala las dependencias del proyecto (incluyendo Symfony)
$ cd demokratian_gestor_censos.git/
$ composer install

```

* Configuration

A medida que se vaya realizando la instalación, la aplicación te ira pidiendo una serie de parámetros que necesitas tener. En caso de que alguno necesites cambiarlo posteriormente tendrás que modificar el archivo   

ruta_a_tu_instalacion app/config/ parameters.yml

Datos que te ira pidiendo la aplicación y su explicación son:

```

Datos de la Base de Datos
    database_host: 127.0.0.1  # host de la base de datos, normalmente localhost o 127.0.0.1
    database_port:            #puerto de la base de datos, generalmente dejar en blanco
    database_name:            # nombre de la base de datos
    database_user:            # usuario de la base de datos  
    database_password:        #passsword de la base de datos

Datos del servidor de correo  
    mailer_transport: smtp
    mailer_host: 127.0.0.1
    mailer_user: ~
    mailer_password: ~
    mailer_encryption: tls
    correoEnvios: ~

Datos de seguridad  
    secret: ThisTokenIsNotSoSecretChangeIt    # A secret key that s used to generate certain security-related tokens

Rutas de las carpetas donde se suben imágenes y tamaños permitidos #######
    saved_file:               # carpeta  donde se suben las imágenes, la url es relativa a la carpeta web, por defecto   uploads/image/
    size_file:                #tamaño máximo de las imágenes , por defecto    1000
    security_saved_file:      # carpeta donde se suben los archivos enviados por los inscritos.  Por  seguridad se suben fuera de la carpeta " web "  para que no sean accesibles desde la red, por defecto uploads/image/
    security_size_file:       #tamaño maximo de los archivos enviados por los inscritos por defecto 1000

Parametros para configurar reCAPTCHA  .  reCAPTCHA es una extensión de la prueba Captcha que se utiliza para reconocer texto presente en imágenes de google. Deberá crear una cuenta  y optener los datos de configuración  aquí https://www.google.com/recaptcha/admin#list
    reCAPTCHA_data_sitekey:            #clave que genera google y se llama data_sitekey
    reCAPTCHA_secret:                  #  clave que genera google y se llama secret

Otros datos  
    subdirectorio:             #si la aplicación está instalada en un subdirectorio o si estamos en pruebas, hay que indicarlo, si no, dejar vacio
    numeroValidadores: 2       # numero de validadores necesarios para que un inscrito se considere validado, por defecto 2

Datos redes sociales
    twitter_data:            # Url de su cuenta de twitter, por defecto: https://twitter.com/csalgadow
    facebook_data:           # Url de su cuenta de facebook, por defecto: https://www.facebook.com/Carlos.Salgado.Werner
    instagram_dada:          # Url de su cuenta de instagram, por defecto:    https://www.instagram.com/

```


* COMPROBAR LOS REQUERIMIENTOS DE SU SERVIDOR

Una vez está hecho lo básico, ahora es el momento de comprobar y asegurar el buen funcionamiento. Existe un código que te permite saber si tu servidor tiene las necesidades básicas o más recomendables para Symfony:
Puede comprobar los requerimientos, de dos formas, mediante su navegador  

Symfony incluye un archivo config.php en el directorio publico  web/ , acceda a el mediante su navegador con  suproyecto/config.php
¡¡¡MUY IMPORTANTE!!! Recuerde borrar siempre el archivo  web/config.php ya que puede generar información sensible que permita ataques a su servidor
También puede comprobar  los requerimientos mediante la consola.

Abra su terminal de la consola, entre en su proyecto  y ejecute el comando

 symfony_requirements:

```
 cd my-project/
 php bin/symfony_requirements

```
* Instalación de la Base de Datos

Instale los datos y tablas de la base de datos Acceda a su gestor e bases de datos en su servidor e importe el archivo BBDD_gestor_censos.sql que encontrara en la raíz de su instalación. Este archivo creara las tablas e incluirá los datos básicos para empezar a funcionar con la aplicación. Por defecto se creara un usuario con nivel de superadministrador

User: info@demokratian.org

password: 65656565

Cambie esos datos como primera acción en cuanto acceda a la aplicación


* Dependencies
* How to run tests
* Deployment instructions


### Como contribuir ###

DEMOKRATIAN es software libre bajo licencia gpl-3.0 por tanto eres libre de usarlo, pero si quieres también puedes colaborar en su desarrollo.
* Si sabes programar en PHP y te apetece mejorar la aplicación y convertirte en contribuidor, contacta con nosotros, todas las manos para mejorar y crear nuevas opciones para DEMOKRATIAN son bienvenidas.
* También puedes colaborar haciendo una donación que nos pueden ayudar a mantener los servidores de desarrollo o animarnos a seguir desarrollando en proyecto.[DONAR con PAYPAL](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=LAC3ZCRAWYAU2&lc=ES&item_name=Donaci%c3%b3n%20a%20%20Demokratian%2eorg&currency_code=EUR&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted)


### Contactar ###

Para contactar puede hacerlo mediante el correo info@demokratian.org
Aun no tenemos equipo de contribuidores así que  no podemos darle más formas de contacto.

### Contribuidores ###
* En el código:
