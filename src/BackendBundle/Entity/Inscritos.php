<?php

namespace BackendBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Inscritos
 * @UniqueEntity(fields={"correoUsuario"}, message="¡Este correo ya esta registrado!")
 * @UniqueEntity(fields={"documento"}, message="¡Este numero de documento ya esta registrado!")
 */
class Inscritos {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message = "Por favor, escribe tu nombre")
     */
    private $nombreUsuario;

    /**
     * @var string
     * @Assert\NotBlank(message = "Por favor, escribe tus apellidos")
     */
    private $apellidoUsuario;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Range(
     *      min = 9,
     *      minMessage = "Este campo tiene que tener  {{ limit }} numeros ",
     * )
     */
    private $telefono;

    /**
     * @var string
     * @Assert\Email(checkMX=true)
     */
    private $correoUsuario;

    /**
     * @var string
     */
    private $validadocorreo = 'NO';

    /**
     * @var string
     */
    private $tipodocumento;

    /**
     * @var string
     * @Assert\NotBlank(message = "Por favor, escribe tu numero de documento")
     */
    private $documento;

    /**
     * @var \DateTime
     * @Assert\NotBlank(message = "Por favor, escribe tu fecha de nacimiento")
     */
    private $fechanacimiento;

    /**
     * @var string
     */
    private $pais;

    /**
     * @var string
     */
    private $provinciaotros;

    /**
     * @var string
     */
    private $municipiootros;

    /**
     * @var string
     * @Assert\NotBlank(message = "Por favor, escribe tu codigo postal")
     */
    private $codigopostal;

    /**
     * @var string
     * @Assert\NotBlank(message = "Por favor, escribe tu direccion de contacto")
     */
    private $direccion;

    /**
     * @var string
     */
    private $lugarparticipacion;

    /**
     * @var \DateTime
     */
    private $fechainscritpcion;

    /**
     * @var string
     */
    private $ipinscripcion;

    /**
     * @var string
     */
    private $cadenaseg;

    /**
     * @var string
     */
    private $bloqueo = 'no';

    /**
     * @var string
     */
    private $razonBloqueo;

    /**
     * @var string
     */
    private $perfil;

    /**
     * @var \DateTime
     */
    private $fechaControl;

    /**
     * @var integer
     */
    private $idMunicipio;

    /**
     * @var integer
     */
    private $idProvincia;

    /**
     * @var string
     */
    private $idCcaa;

    public function __toString() {
        return (string) $this->getId();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nombreUsuario
     *
     * @param string $nombreUsuario
     *
     * @return Inscritos
     */
    public function setNombreUsuario($nombreUsuario) {
        $this->nombreUsuario = $nombreUsuario;

        return $this;
    }

    /**
     * Get nombreUsuario
     *
     * @return string
     */
    public function getNombreUsuario() {
        return $this->nombreUsuario;
    }

    /**
     * Set apellidoUsuario
     *
     * @param string $apellidoUsuario
     *
     * @return Inscritos
     */
    public function setApellidoUsuario($apellidoUsuario) {
        $this->apellidoUsuario = $apellidoUsuario;

        return $this;
    }

    /**
     * Get apellidoUsuario
     *
     * @return string
     */
    public function getApellidoUsuario() {
        return $this->apellidoUsuario;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Inscritos
     */
    public function setTelefono($telefono) {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono() {
        return $this->telefono;
    }

    /**
     * Set correoUsuario
     *
     * @param string $correoUsuario
     *
     * @return Inscritos
     */
    public function setCorreoUsuario($correoUsuario) {
        $this->correoUsuario = $correoUsuario;

        return $this;
    }

    /**
     * Get correoUsuario
     *
     * @return string
     */
    public function getCorreoUsuario() {
        return $this->correoUsuario;
    }

    /**
     * Set validadocorreo
     *
     * @param string $validadocorreo
     *
     * @return Inscritos
     */
    public function setValidadocorreo($validadocorreo) {
        $this->validadocorreo = $validadocorreo;

        return $this;
    }

    /**
     * Get validadocorreo
     *
     * @return string
     */
    public function getValidadocorreo() {
        return $this->validadocorreo;
    }

    /**
     * Set tipodocumento
     *
     * @param string $tipodocumento
     *
     * @return Inscritos
     */
    public function setTipodocumento($tipodocumento) {
        $this->tipodocumento = $tipodocumento;

        return $this;
    }

    /**
     * Get tipodocumento
     *
     * @return string
     */
    public function getTipodocumento() {
        return $this->tipodocumento;
    }

    /**
     * Set documento
     *
     * @param string $documento
     *
     * @return Inscritos
     */
    public function setDocumento($documento) {
        $this->documento = $documento;

        return $this;
    }

    /**
     * Get documento
     *
     * @return string
     */
    public function getDocumento() {
        return $this->documento;
    }

    /**
     * Set fechanacimiento
     *
     * @param \DateTime $fechanacimiento
     *
     * @return Inscritos
     */
    public function setFechanacimiento($fechanacimiento) {
        $this->fechanacimiento = $fechanacimiento;

        return $this;
    }

    /**
     * Get fechanacimiento
     *
     * @return \DateTime
     */
    public function getFechanacimiento() {
        return $this->fechanacimiento;
    }

    /**
     * Set pais
     *
     * @param string $pais
     *
     * @return Inscritos
     */
    public function setPais($pais) {
        $this->pais = $pais;

        return $this;
    }

    /**
     * Get pais
     *
     * @return string
     */
    public function getPais() {
        return $this->pais;
    }

    /**
     * Set provinciaotros
     *
     * @param string $provinciaotros
     *
     * @return Inscritos
     */
    public function setProvinciaotros($provinciaotros) {
        $this->provinciaotros = $provinciaotros;

        return $this;
    }

    /**
     * Get provinciaotros
     *
     * @return string
     */
    public function getProvinciaotros() {
        return $this->provinciaotros;
    }

    /**
     * Set municipiootros
     *
     * @param string $municipiootros
     *
     * @return Inscritos
     */
    public function setMunicipiootros($municipiootros) {
        $this->municipiootros = $municipiootros;

        return $this;
    }

    /**
     * Get municipiootros
     *
     * @return string
     */
    public function getMunicipiootros() {
        return $this->municipiootros;
    }

    /**
     * Set codigopostal
     *
     * @param string $codigopostal
     *
     * @return Inscritos
     */
    public function setCodigopostal($codigopostal) {
        $this->codigopostal = $codigopostal;

        return $this;
    }

    /**
     * Get codigopostal
     *
     * @return string
     */
    public function getCodigopostal() {
        return $this->codigopostal;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return Inscritos
     */
    public function setDireccion($direccion) {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion() {
        return $this->direccion;
    }

    /**
     * Set lugarparticipacion
     *
     * @param string $lugarparticipacion
     *
     * @return Inscritos
     */
    public function setLugarparticipacion($lugarparticipacion) {
        $this->lugarparticipacion = $lugarparticipacion;

        return $this;
    }

    /**
     * Get lugarparticipacion
     *
     * @return string
     */
    public function getLugarparticipacion() {
        return $this->lugarparticipacion;
    }

    /**
     * Set fechainscritpcion
     *
     * @param \DateTime $fechainscritpcion
     *
     * @return Inscritos
     */
    public function setFechainscritpcion($fechainscritpcion) {
        $this->fechainscritpcion = $fechainscritpcion;

        return $this;
    }

    /**
     * Get fechainscritpcion
     *
     * @return \DateTime
     */
    public function getFechainscritpcion() {
        return $this->fechainscritpcion;
    }

    /**
     * Set ipinscripcion
     *
     * @param string $ipinscripcion
     *
     * @return Inscritos
     */
    public function setIpinscripcion($ipinscripcion) {
        $this->ipinscripcion = $ipinscripcion;

        return $this;
    }

    /**
     * Get ipinscripcion
     *
     * @return string
     */
    public function getIpinscripcion() {
        return $this->ipinscripcion;
    }

    /**
     * Set cadenaseg
     *
     * @param string $cadenaseg
     *
     * @return Inscritos
     */
    public function setCadenaseg($cadenaseg) {
        $this->cadenaseg = $cadenaseg;

        return $this;
    }

    /**
     * Get cadenaseg
     *
     * @return string
     */
    public function getCadenaseg() {
        return $this->cadenaseg;
    }

    /**
     * Set bloqueo
     *
     * @param string $bloqueo
     *
     * @return Inscritos
     */
    public function setBloqueo($bloqueo) {
        $this->bloqueo = $bloqueo;

        return $this;
    }

    /**
     * Get bloqueo
     *
     * @return string
     */
    public function getBloqueo() {
        return $this->bloqueo;
    }

    /**
     * Set razonBloqueo
     *
     * @param string $razonBloqueo
     *
     * @return Inscritos
     */
    public function setRazonBloqueo($razonBloqueo) {
        $this->razonBloqueo = $razonBloqueo;

        return $this;
    }

    /**
     * Get razonBloqueo
     *
     * @return string
     */
    public function getRazonBloqueo() {
        return $this->razonBloqueo;
    }

    /**
     * Set perfil
     *
     * @param string $perfil
     *
     * @return Inscritos
     */
    public function setPerfil($perfil) {
        $this->perfil = $perfil;

        return $this;
    }

    /**
     * Get perfil
     *
     * @return string
     */
    public function getPerfil() {
        return $this->perfil;
    }

    /**
     * Set fechaControl
     *
     * @param \DateTime $fechaControl
     *
     * @return Inscritos
     */
    public function setFechaControl($fechaControl) {
        $this->fechaControl = $fechaControl;

        return $this;
    }

    /**
     * Get fechaControl
     *
     * @return \DateTime
     */
    public function getFechaControl() {
        return $this->fechaControl;
    }

    /**
     * Set idMunicipio
     *
     * @param integer $idMunicipio
     *
     * @return Inscritos
     */
    public function setIdMunicipio($idMunicipio) {
        $this->idMunicipio = $idMunicipio;

        return $this;
    }

    /**
     * Get idMunicipio
     *
     * @return integer
     */
    public function getIdMunicipio() {
        return $this->idMunicipio;
    }

    /**
     * Set idProvincia
     *
     * @param integer $idProvincia
     *
     * @return Inscritos
     */
    public function setIdProvincia($idProvincia) {
        $this->idProvincia = $idProvincia;

        return $this;
    }

    /**
     * Get idProvincia
     *
     * @return integer
     */
    public function getIdProvincia() {
        return $this->idProvincia;
    }

    /**
     * Set idCcaa
     *
     * @param string $idCcaa
     *
     * @return Inscritos
     */
    public function setIdCcaa($idCcaa) {
        $this->idCcaa = $idCcaa;

        return $this;
    }

    /**
     * Get idCcaa
     *
     * @return string
     */
    public function getIdCcaa() {
        return $this->idCcaa;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $imagenes;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $validaInscritos;

    /**
     * @var \BackendBundle\Entity\Ccaa
     */
    private $ccaaInscritos;

    /**
     * @var \BackendBundle\Entity\Municipios
     */
    private $municipioInscritos;

    /**
     * @var \BackendBundle\Entity\Provincia
     */
    private $provinciaInscritos;

    /**
     * Constructor
     */
    public function __construct() {
        $this->imagenes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->validaInscritos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add imagene
     *
     * @param \BackendBundle\Entity\InscritosImages $imagene
     *
     * @return Inscritos
     */
    public function addImagene(\BackendBundle\Entity\InscritosImages $imagene) {
        $this->imagenes[] = $imagene;

        return $this;
    }

    /**
     * Remove imagene
     *
     * @param \BackendBundle\Entity\InscritosImages $imagene
     */
    public function removeImagene(\BackendBundle\Entity\InscritosImages $imagene) {
        $this->imagenes->removeElement($imagene);
    }

    /**
     * Get imagenes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImagenes() {
        return $this->imagenes;
    }

    /**
     * Add validaInscrito
     *
     * @param \BackendBundle\Entity\ValidaInscritos $validaInscrito
     *
     * @return Inscritos
     */
    public function addValidaInscrito(\BackendBundle\Entity\ValidaInscritos $validaInscrito) {
        $this->validaInscritos[] = $validaInscrito;

        return $this;
    }

    /**
     * Remove validaInscrito
     *
     * @param \BackendBundle\Entity\ValidaInscritos $validaInscrito
     */
    public function removeValidaInscrito(\BackendBundle\Entity\ValidaInscritos $validaInscrito) {
        $this->validaInscritos->removeElement($validaInscrito);
    }

    /**
     * Get validaInscritos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getValidaInscritos() {
        return $this->validaInscritos;
    }

    /**
     * Set ccaaInscritos
     *
     * @param \BackendBundle\Entity\Ccaa $ccaaInscritos
     *
     * @return Inscritos
     */
    public function setCcaaInscritos(\BackendBundle\Entity\Ccaa $ccaaInscritos = null) {
        $this->ccaaInscritos = $ccaaInscritos;

        return $this;
    }

    /**
     * Get ccaaInscritos
     *
     * @return \BackendBundle\Entity\Ccaa
     */
    public function getCcaaInscritos() {
        return $this->ccaaInscritos;
    }

    /**
     * Set municipioInscritos
     *
     * @param \BackendBundle\Entity\Municipios $municipioInscritos
     *
     * @return Inscritos
     */
    public function setMunicipioInscritos(\BackendBundle\Entity\Municipios $municipioInscritos = null) {
        $this->municipioInscritos = $municipioInscritos;

        return $this;
    }

    /**
     * Get municipioInscritos
     *
     * @return \BackendBundle\Entity\Municipios
     */
    public function getMunicipioInscritos() {
        return $this->municipioInscritos;
    }

    /**
     * Set provinciaInscritos
     *
     * @param \BackendBundle\Entity\Provincia $provinciaInscritos
     *
     * @return Inscritos
     */
    public function setProvinciaInscritos(\BackendBundle\Entity\Provincia $provinciaInscritos = null) {
        $this->provinciaInscritos = $provinciaInscritos;

        return $this;
    }

    /**
     * Get provinciaInscritos
     *
     * @return \BackendBundle\Entity\Provincia
     */
    public function getProvinciaInscritos() {
        return $this->provinciaInscritos;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $inscritosAcciones;

    /**
     * Add inscritosAccione
     *
     * @param \BackendBundle\Entity\InscritosAcciones $inscritosAccione
     *
     * @return Inscritos
     */
    public function addInscritosAccione(\BackendBundle\Entity\InscritosAcciones $inscritosAccione) {
        $this->inscritosAcciones[] = $inscritosAccione;

        return $this;
    }

    /**
     * Remove inscritosAccione
     *
     * @param \BackendBundle\Entity\InscritosAcciones $inscritosAccione
     */
    public function removeInscritosAccione(\BackendBundle\Entity\InscritosAcciones $inscritosAccione) {
        $this->inscritosAcciones->removeElement($inscritosAccione);
    }

    /**
     * Get inscritosAcciones
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInscritosAcciones() {
        return $this->inscritosAcciones;
    }

}
