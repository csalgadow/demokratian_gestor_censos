<?php

namespace BackendBundle\Entity;

/**
 * InscritosImages
 */
class InscritosImages
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $idInscrito;

    /**
     * @var string
     */
    private $images;

    /**
     * @var \DateTime
     */
    private $fecha = 'CURRENT_TIMESTAMP';

    /**
     * @var \BackendBundle\Entity\Inscritos
     */
    private $inscritos;


    /**
     * Set id
     *
     * @param integer $id
     *
     * @return InscritosImages
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idInscrito
     *
     * @param string $idInscrito
     *
     * @return InscritosImages
     */
    public function setIdInscrito($idInscrito)
    {
        $this->idInscrito = $idInscrito;

        return $this;
    }

    /**
     * Get idInscrito
     *
     * @return string
     */
    public function getIdInscrito()
    {
        return $this->idInscrito;
    }

    /**
     * Set images
     *
     * @param string $images
     *
     * @return InscritosImages
     */
    public function setImages($images)
    {
        $this->images = $images;

        return $this;
    }

    /**
     * Get images
     *
     * @return string
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return InscritosImages
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set inscritos
     *
     * @param \BackendBundle\Entity\Inscritos $inscritos
     *
     * @return InscritosImages
     */
    public function setInscritos(\BackendBundle\Entity\Inscritos $inscritos = null)
    {
        $this->inscritos = $inscritos;

        return $this;
    }

    /**
     * Get inscritos
     *
     * @return \BackendBundle\Entity\Inscritos
     */
    public function getInscritos()
    {
        return $this->inscritos;
    }
}
