<?php

namespace BackendBundle\Entity;

/**
 * Municipios
 */
class Municipios
{
    /**
     * @var integer
     */
    private $idMunicipio;

    /**
     * @var integer
     */
    private $codMunicipio;

    /**
     * @var integer
     */
    private $dc;

    /**
     * @var string
     */
    private $nombre = '';

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userMunicipio;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $inscritosMunicipio;

    /**
     * @var \BackendBundle\Entity\Provincia
     */
    /**
     * @ORM\ManyToOne(targetEntity="BackendBundle\Entity\Provincia", inversedBy="municipio")
     * @ORM\JoinColumn(name="municipio_id", referencedColumnName="id")
     * @Assert\NotBlank
     */
    private $idProvincia;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userMunicipio = new \Doctrine\Common\Collections\ArrayCollection();
        $this->inscritosMunicipio = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idMunicipio= new \Doctrine\Common\Collections\ArrayCollection;
        $this->idProvincia= new \Doctrine\Common\Collections\ArrayCollection;
    }

     public function __toString() {
       // return strval($this->idCcaa);
        return $this->nombre;
    }
    
    /**
     * Set idMunicipio
     *
     * @param integer $idMunicipio
     *
     * @return Municipios
     */
    public function setIdMunicipio($idMunicipio)
    {
        $this->idMunicipio = $idMunicipio;

        return $this;
    }

    /**
     * Get idMunicipio
     *
     * @return integer
     */
    public function getIdMunicipio()
    {
        return $this->idMunicipio;
    }

    /**
     * Set codMunicipio
     *
     * @param integer $codMunicipio
     *
     * @return Municipios
     */
    public function setCodMunicipio($codMunicipio)
    {
        $this->codMunicipio = $codMunicipio;

        return $this;
    }

    /**
     * Get codMunicipio
     *
     * @return integer
     */
    public function getCodMunicipio()
    {
        return $this->codMunicipio;
    }

    /**
     * Set dc
     *
     * @param integer $dc
     *
     * @return Municipios
     */
    public function setDc($dc)
    {
        $this->dc = $dc;

        return $this;
    }

    /**
     * Get dc
     *
     * @return integer
     */
    public function getDc()
    {
        return $this->dc;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Municipios
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add userMunicipio
     *
     * @param \BackendBundle\Entity\Users $userMunicipio
     *
     * @return Municipios
     */
    public function addUserMunicipio(\BackendBundle\Entity\Users $userMunicipio)
    {
        $this->userMunicipio[] = $userMunicipio;

        return $this;
    }

    /**
     * Remove userMunicipio
     *
     * @param \BackendBundle\Entity\Users $userMunicipio
     */
    public function removeUserMunicipio(\BackendBundle\Entity\Users $userMunicipio)
    {
        $this->userMunicipio->removeElement($userMunicipio);
    }

    /**
     * Get userMunicipio
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserMunicipio()
    {
        return $this->userMunicipio;
    }

    /**
     * Add inscritosMunicipio
     *
     * @param \BackendBundle\Entity\Inscritos $inscritosMunicipio
     *
     * @return Municipios
     */
    public function addInscritosMunicipio(\BackendBundle\Entity\Inscritos $inscritosMunicipio)
    {
        $this->inscritosMunicipio[] = $inscritosMunicipio;

        return $this;
    }

    /**
     * Remove inscritosMunicipio
     *
     * @param \BackendBundle\Entity\Inscritos $inscritosMunicipio
     */
    public function removeInscritosMunicipio(\BackendBundle\Entity\Inscritos $inscritosMunicipio)
    {
        $this->inscritosMunicipio->removeElement($inscritosMunicipio);
    }

    /**
     * Get inscritosMunicipio
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInscritosMunicipio()
    {
        return $this->inscritosMunicipio;
    }

    /**
     * Set idProvincia
     *
     * @param \BackendBundle\Entity\Provincia $idProvincia
     *
     * @return Municipios
     */
    public function setIdProvincia(\BackendBundle\Entity\Provincia $idProvincia = null)
    {
        $this->idProvincia = $idProvincia;

        return $this;
    }

    /**
     * Get idProvincia
     *
     * @return \BackendBundle\Entity\Provincia
     */
    public function getIdProvincia()
    {
        return $this->idProvincia;
    }
}
