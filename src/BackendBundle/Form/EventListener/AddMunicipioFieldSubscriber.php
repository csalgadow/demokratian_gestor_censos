<?php
//AddMunicipioFieldSubscriber
namespace BackendBundle\Form\EventListener;

use BackendBundle\Entity\Municipios;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class AddMunicipioFieldSubscriber implements EventSubscriberInterface {

    private $propertyPathToMunicipio;

    public function __construct($propertyPathToMunicipio) {
        $this->propertyPathToMunicipio = $propertyPathToMunicipio;
    }

    public static function getSubscribedEvents() {
        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT => 'preSubmit'
        );
    }

    private function addCityForm($form, $province_id) {
        $formOptions = array(
            'class' => 'BackendBundle:Municipios',
            'label' => 'Municipio',
            "required" => "required",
            'attr' => array(
                'class' => 'form-name form-control class_select_municipio',
            ),
            'query_builder' => function (EntityRepository $repository) use ($province_id) {
                $qb = $repository->createQueryBuilder('municipios')
                        ->innerJoin('municipios.idProvincia', 'provincia')
                        ->where('provincia.id = :provincia')
                        ->setParameter('provincia', $province_id)
                ;

                return $qb;
            }
        );

        $form->add($this->propertyPathToMunicipio, EntityType::class, $formOptions);
    }

    public function preSetData(FormEvent $event) {
        $data = $event->getData();
        $form = $event->getForm();

        if (null === $data) {
            return;
        }

        $accessor = PropertyAccess::createPropertyAccessor();

        $municipios = $accessor->getValue($data, $this->propertyPathToMunicipio);
        $province_id = ($municipios) ? $municipios->getIdMunicipio()->getProvincia()->getId() : null;

        $this->addCityForm($form, $province_id);
    }

    public function preSubmit(FormEvent $event) {
        $data = $event->getData();
        $form = $event->getForm();

        $province_id = array_key_exists('provincia', $data) ? $data['provincia'] : null;

        $this->addCityForm($form, $province_id);
    }

}
