<?php

namespace BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class InscritosAccionesType extends AbstractType {

    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage) {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                // ->add('status')
                ->add('status', EntityType::class, array("label" => "Acción: ",
                    'required' => false,
                    "class" => "BackendBundle:Acciones",
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                                ->orderBy('u.acciones', 'ASC');
                    },
                    'placeholder' => 'Selecciona una acción',
                    "attr" => array("class" => "form-name form-control col-sm-4 col-md-3")))
                ->add('accion', TextareaType::class, array("label" => "Acción: ",
                    "required" => "required",
                    "attr" => array("class" => "form-name form-control col-sm-4 col-md-3")))
                ->add('datos', TextareaType::class, array("label" => "Datos: ",
                    "required" => "required",
                    "attr" => array("class" => "form-name form-control col-sm-4 col-md-3")))
        ;

        // grab the user, do a quick sanity check that one exists
        $user = $this->tokenStorage->getToken()->getUser();
        if (!$user) {
            throw new \LogicException(
            'The FriendMessageFormType cannot be used without an authenticated user!'
            );
        }

        $builder->addEventListener(
                FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($user) {
            $form = $event->getForm();

            $formOptions = array(
                'class' => "BackendBundle:Acciones",
                'choice_label' => 'fullName',
                'query_builder' => function (EntityRepository $er) use ($user) {
                    //build a custom query
                    return $er->createQueryBuilder('u')->addOrderBy('status', 'DESC');

                    // or call a method on your repository that returns the query builder
                    // the $er is an instance of your UserRepository
                    // return $er->createOrderByFullNameQueryBuilder();
                },
            );

            // create the field, this is similar the $builder->add()
            // field name, field type, data, options
            $form->add('friend', EntityType::class, $formOptions);
        }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'BackendBundle\Entity\InscritosAcciones'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'backendbundle_inscritosacciones';
    }

}
