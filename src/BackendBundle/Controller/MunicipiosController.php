<?php

namespace BackendBundle\Controller;

use BackendBundle\Entity\Municipios;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Municipio controller.
 *
 */
class MunicipiosController extends Controller
{
    /**
     * Lists all municipio entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $municipios = $em->getRepository('BackendBundle:Municipios')->findAll();

        return $this->render('BackendBundle:municipios:index.html.twig', array(
            'municipios' => $municipios,
        ));
    }

    /**
     * Finds and displays a municipio entity.
     *
     */
    public function showAction(Municipios $municipio)
    {

        return $this->render('BackendBundle:municipios:show.html.twig', array(
            'municipio' => $municipio,
        ));
    }
}
