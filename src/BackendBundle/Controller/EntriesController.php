<?php

namespace BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use BackendBundle\Entity\Entries;
use BackendBundle\Form\EntriesType;
use BackendBundle\Repository\resizeImageRepository;

/**
 * Entry controller.
 *
 */
class EntriesController extends Controller {

    private $session;

    public function __construct() {
        $this->session = new Session();
    }

    /**
     * Lists all entry entities.
     *
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $dql = "SELECT e FROM BackendBundle:Entries e";
        $query = $em->createQuery($dql);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, $request->query->getInt('page', 1), 15
        );
        //$entries = $em->getRepository('BackendBundle:Entries')->findAll();

        return $this->render('BackendBundle:entries:index.html.twig', array('pagination' => $pagination)
        );
    }

    /**
     * Creates a new entry entity.
     *
     */
    public function newAction(Request $request) {
        $entry = new Entries();
        $form = $this->createForm('BackendBundle\Form\EntriesType', $entry);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $status = false;
                $em = $this->getDoctrine()->getManager();
                $entry_repo = $em->getRepository("BackendBundle:Entries");
                $category_repo = $em->getRepository("BackendBundle:Categories");
                $entry = new Entries();


                //subimos la imagen si se ha enviado

                $file = $form["image"]->getData(); //cogemos los datos de la imagen
                if (!empty($file) && $file != null) {
                    $ext = $file->guessExtension(); //miramos la extension
                    $file_name = $file->getClientOriginalName();
                    $file_size = $file->getClientSize(); //miramos el tamaño
                    $file_type = $file->getMimeType(); // miramos el tipo de archivo que es para comprobar que es una imagen
                    $size_file = $this->getParameter('size_file');
                    $maxsize = $size_file * 1024;
                    if ($file_size >= $maxsize) {  //miramos a ver si excede el tamaño permitido
                        $file_size = ceil($file_size / 1024);
                        $this->session->getFlashBag()->add('warning', 'La imagen es demasiado grande. El tamaño maximo es de : ' . $size_file . ' kb y su archivo tiene: ' . $file_size . ' kb');
                    } else {

                        if ($file_type == "image/jpg" || $file_type == "image/jpeg" || $file_type == "image/png" || $file_type == "image/gif") {

                            $t = 0;
                            $nombre_base = basename($file_name, $ext); //quitamos la extension
                            $nombre_base = substr($nombre_base, 0, -1); //quitamos el punto que se queda
                            while (file_exists($this->getParameter('upload_file') . $file_name)) {

                                $actual_name = $nombre_base . "_" . $t;
                                $file_name = $actual_name . "." . $ext;
                                $t++;
                            }



                            $file->move($this->getParameter('upload_file'), $file_name); //subimos el archivo a la carpeta que tenemos definida en config.yml como upload_file que estara dentro de la carpeta web

                            $entry->setImage($file_name);
                            $status = "ok";  //avisamos que la imagen se ha subido bien
                            //Un ejemplo de uso:
                            //Indicamos la ruta a la imagen alojada en nuestro servidor
                            $file_grande = $this->getParameter('upload_file') . '/' . $file_name;
                            //Indicamos la ruta de destino
                            $resizedFile = $this->getParameter('upload_file') . '/peq/' . $file_name;
                            //Llamamos a la función encargada de redimensionar la imagen
                            $myImageResizer = new resizeImageRepository();
                            $dato_image = $myImageResizer->smart_resize_image($file_grande, null, 250, 0, true, $resizedFile, false, false, 100);
                        } else {
                            $this->session->getFlashBag()->add('warning', 'El archivo enviado no es una imagen, es del tipo: ' . $file_type);
                        }
                    }
                } else { //si no hemos añadido imagen
                    $entry->setImage(null);
                    $status = "ok";  //avisamos que se pueden añadir el resto de items
                }

                if ($status == "ok") {
                    $entry->setTitle($form->get("title")->getData());
                    $entry->setContent($form->get("content")->getData());
                    $entry->setStatus($form->get("status")->getData());
                    $category = $category_repo->find($form->get("category")->getData());
                    $entry->setCategory($category);

                    $user = $this->getUser();
                    $entry->setUser($user);

                    $em->persist($entry);
                    $flush = $em->flush();


                    if ($flush === null) {
                        $this->session->getFlashBag()->add('success', 'La entrada ' . $form->get("title")->getData() . 'se ha creado correctamente ');
                        return $this->redirectToRoute("entries_index"); //redirigimos la pagina si se incluido correctamete
                    } else {
                        $this->session->getFlashBag()->add('warning', 'Error al añadir la entrada');
                    }
                }
            } else {
                $this->session->getFlashBag()->add('warning', 'La entrada no se ha creado por un error en el formulario !');
            }
        }


        return $this->render('BackendBundle:entries:new.html.twig', array(
                    'entry' => $entry,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a entry entity.
     *
     */
    public function showAction(Entries $entry) {
        $deleteForm = $this->createDeleteForm($entry);

        return $this->render('BackendBundle:entries:show.html.twig', array(
                    'entry' => $entry,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing entry entity.
     *
     */
    public function editAction(Request $request, Entries $entry) {

        $em = $this->getDoctrine()->getManager();
        $entry_repo = $em->getRepository("BackendBundle:Entries");
        $category_repo = $em->getRepository("BackendBundle:Categories");
        $entry = $entry_repo->find($entry);
        $entry_image = $entry->getImage(); //cogemos el nombre de la imagen por si hay que borarla o hay que volver a meterla 

        $deleteForm = $this->createDeleteForm($entry);
        $editForm = $this->createForm('BackendBundle\Form\EntriesEditType', $entry);
        $editForm->handleRequest($request);


        if ($editForm->isSubmitted()) {
            if ($editForm->isValid()) {
                $status = false;
       //comienzo sistema de subida de la imagen si se ha enviado  
                $file = $editForm["image"]->getData(); //cogemos los datos de la imagen
                if (!empty($file) && $file != null) {
                    if (file_exists($this->getParameter('upload_file') . $entry_image) && $entry_image != null) { //si hay una imagen en la base de datos y el el servidor la borramos
                        unlink($this->getParameter('upload_file') . $entry_image); //borramos la imagen antigua
                    }
                    if (file_exists($this->getParameter('upload_file') . 'peq/' . $entry_image) && $entry_image != null) { //si hay una imagen pequeña en la base de datos y el el servidor la borramos
                        unlink($this->getParameter('upload_file') . 'peq/' . $entry_image); //borramos la pequeña imagen antigua
                    }

                    $ext = $file->guessExtension(); //miramos la extension
                    $file_name = $file->getClientOriginalName();
                    $file_size = $file->getClientSize(); //miramos el tamaño
                    $file_type = $file->getMimeType(); // miramos el tipo de archivo que es para comprobar que es una imagen
                    $size_file = $this->getParameter('size_file');
                    $maxsize = $size_file * 1024;
                    if ($file_size >= $maxsize) {  //miramos a ver si excede el tamaño permitido
                        $file_size = ceil($file_size / 1024);
                        $this->session->getFlashBag()->add('warning', 'La imagen es demasiado grande. El tamaño maximo es de : ' . $size_file . ' kb y su archivo tiene: ' . $file_size . ' kb');
                    } else {

                        if ($file_type == "image/jpg" || $file_type == "image/jpeg" || $file_type == "image/png" || $file_type == "image/gif") {

                            $t = 0;
                            $nombre_base = basename($file_name, $ext); //quitamos la extension
                            $nombre_base = substr($nombre_base, 0, -1); //quitamos el punto que se queda
                            while (file_exists($this->getParameter('upload_file') . $file_name)) {

                                $actual_name = $nombre_base . "_" . $t;
                                $file_name = $actual_name . "." . $ext;
                                $t++;
                            }



                            $file->move($this->getParameter('upload_file'), $file_name); //subimos el archivo a la carpeta que tenemos definida en config.yml como upload_file que estara dentro de la carpeta web

                            $entry->setImage($file_name);
                            $status = "ok";  //avisamos que la imagen se ha subido bien
                            //Un ejemplo de uso:
                            //Indicamos la ruta a la imagen alojada en nuestro servidor
                            $file_grande = $this->getParameter('upload_file') . '/' . $file_name;
                            //Indicamos la ruta de destino
                            $resizedFile = $this->getParameter('upload_file') . '/peq/' . $file_name;
                            //Llamamos a la función encargada de redimensionar la imagen
                            $myImageResizer = new resizeImageRepository();
                            $dato_image = $myImageResizer->smart_resize_image($file_grande, null, 250, 0, true, $resizedFile, false, false, 100);
                        } else {
                            $this->session->getFlashBag()->add('warning', 'El archivo envisdo no es una imagen, es del tipo: ' . $file_type);
                        }
                    }
                } else { //si no hemos añadido imagen nueva
                    $acepto = $request->request->get('acepto'); //miramos si hemos chequeado para borrar la imagen
                    if ($acepto == 1) {  // si hemos marcado para borrar la imagen
                        if (file_exists($this->getParameter('upload_file') . $entry_image) && $entry_image != null) { //si hay una imagen en la base de datos y el el servidor la borramos
                            unlink($this->getParameter('upload_file') . $entry_image); //borramos la imagen antigua
                        }
                        if (file_exists($this->getParameter('upload_file') . 'peq/' . $entry_image) && $entry_image != null) { //si hay una imagen pequeña en la base de datos y el el servidor la borramos
                            unlink($this->getParameter('upload_file') . 'peq/' . $entry_image); //borramos la pequeña imagen antigua
                        }
                        $entry->setImage(null); // y decimos que ponga null en la base de datos
                    } else {  // si no hemos marcado volvemos a meter el la bbdd el nombre de la imagen
                        $entry->setImage($entry_image);
                    }
                    $status = "ok";  //avisamos que se pueden añadir el resto de items
                }

                /////////  Fin del sistema de añadido de imagenes


                if ($status == "ok") {
                    $entry->setTitle($editForm->get("title")->getData());
                    $entry->setContent($editForm->get("content")->getData());
                    $entry->setStatus($editForm->get("status")->getData());
                    $category = $category_repo->find($editForm->get("category")->getData());
                    $entry->setCategory($category);

                    $user = $this->getUser();
                    $entry->setUser($user);

                    $em->persist($entry);
                    $flush = $em->flush();

                    if ($flush === null) {
                        $this->session->getFlashBag()->add('success', 'La entrada ' . $editForm->get("title")->getData() . ' se ha modificado correctamente ');
                        return $this->redirectToRoute('entries_edit', array('id' => $entry->getId()));
                        //return $this->redirectToRoute("demokratian_index_entry"); //redirigimos la pagina si se incluido correctamete
                    } else {
                        $this->session->getFlashBag()->add('warning', 'Error al añadir la entrada');
                    }
                }
            } else {
                $this->session->getFlashBag()->add('warning', 'La entrada no se ha modificado por un error en el formulario !');
            }




            // $this->getDoctrine()->getManager()->flush();
            //  return $this->redirectToRoute('entries_edit', array('id' => $entry->getId()));
        }

        return $this->render('BackendBundle:entries:edit.html.twig', array(
                    'entry' => $entry,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a entry entity.
     *
     */
    public function deleteAction(Request $request, Entries $entry) {
        $form = $this->createDeleteForm($entry);
        $form->handleRequest($request);
        /// FALTA EL BORRADO DE LA IMAGEN
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($entry);
            $em->flush();
        }

        return $this->redirectToRoute('entries_index');
    }

    /**
     * Creates a form to delete a entry entity.
     *
     * @param Entries $entry The entry entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Entries $entry) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('entries_delete', array('id' => $entry->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    public function categoryAction($id, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $category_repo = $em->getRepository("BackendBundle:Categories");
        $category = $category_repo->find($id);

        //$pageSize = 10; // numero de items por pagina
        // $entry_repo = $em->getRepository("BackendBundle:Entry");
        //$entries = $entry_repo->findBy(array(
        //	'category' => $id));// hay que llamar a los que tienen categoria seguun el id

        $dql = "SELECT e FROM BackendBundle:Entries e where e.category=$id";
        $query = $em->createQuery($dql);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, $request->query->getInt('page', 1), 15
        );

        return $this->render("BackendBundle:Entries:categoryEntries.html.twig", array(
                    "category" => $category,
                    'pagination' => $pagination
                        // "totalItems" => $totalItems,
                        // "pagesCount" => $pagesCount,
                        // "page" => $page
        ));
    }

}
