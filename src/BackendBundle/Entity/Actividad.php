<?php

namespace BackendBundle\Entity;

/**
 * Actividad
 */
class Actividad
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $name;

    /**
     * @var string
     */
    private $texto;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $actividadUser;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->actividadUser = new \Doctrine\Common\Collections\ArrayCollection();
    }

    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set name
     *
     * @param integer $name
     *
     * @return Actividad
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return integer
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set texto
     *
     * @param string $texto
     *
     * @return Actividad
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto
     *
     * @return string
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * Add actividadUser
     *
     * @param \BackendBundle\Entity\ActividadUser $actividadUser
     *
     * @return Actividad
     */
    public function addActividadUser(\BackendBundle\Entity\ActividadUser $actividadUser)
    {
        $this->actividadUser[] = $actividadUser;

        return $this;
    }

    /**
     * Remove actividadUser
     *
     * @param \BackendBundle\Entity\ActividadUser $actividadUser
     */
    public function removeActividadUser(\BackendBundle\Entity\ActividadUser $actividadUser)
    {
        $this->actividadUser->removeElement($actividadUser);
    }

    /**
     * Get actividadUser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActividadUser()
    {
        return $this->actividadUser;
    }
    

}
