<?php

namespace BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class InscritosImagesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('id')
                ->add('idInscrito')
                ->add('images')
                //->add('fecha')
//                ->add('fecha', DateType::class, array("label" => "Fecha: ",
//                    "required" => "required",
//                    'widget' => 'single_text',
//                    // this is actually the default format for single_text
//                    'format' => 'yyyy-MM-dd',
//                
//                    "attr" => array("class" => "form-dateform-control")))
                ->add('inscritos');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackendBundle\Entity\InscritosImages'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backendbundle_inscritosimages';
    }


}
