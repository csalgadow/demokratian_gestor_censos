<?php

namespace AppBundle\Repository;

class security_encript {

    private $iv = '1234567890123456789012345678901234567890123456789012345678901234';
    private $key1 = '123456789abcde123456789';

    public function encrypt($text, $salt = NULL) {
        $salt = (is_null($salt)) ? $this->key1 : $salt;

        if (function_exists('openssl_encrypt')) {
            return rtrim(strtr(base64_encode(openssl_encrypt($text, 'aes-256-cbc', $salt, true, substr($this->iv, 32, 16))), '+/', '-_'), '=');
        } else {
            return trim(rtrim(strtr(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $text, MCRYPT_MODE_CBC, pack('H*', $this->iv))), '+/', '-_'), '='));
        }
    }

    public function decrypt($text, $salt = NULL) {
        $salt = (is_null($salt)) ? $this->key1 : $salt;

        if (function_exists('openssl_decrypt')) {
            return openssl_decrypt(base64_decode(str_pad(strtr($text, '-_', '+/'), strlen($text) % 4, '=', STR_PAD_RIGHT)), 'aes-256-cbc', $salt, true, substr($this->iv, 32, 16));
        } else {
            return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode(str_pad(strtr($text, '-_', '+/'), strlen($text) % 4, '=', STR_PAD_RIGHT)), MCRYPT_MODE_CBC, pack('H*', $this->iv)));
        }
    }

    function base64url_encode($data) {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    function base64url_decode($data) {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }

}
