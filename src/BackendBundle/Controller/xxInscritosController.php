<?php

namespace BackendBundle\Controller;

use BackendBundle\Entity\Inscritos;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Inscrito controller.
 *
 */
class InscritosController extends Controller
{
        private $session;

    public function __construct() {
        $this->session = new Session();
    }
    
    /**
     * Lists all inscrito entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $inscritos = $em->getRepository('BackendBundle:Inscritos')->findAll();

        return $this->render('BackendBundle:inscritos:index.html.twig', array(
            'inscritos' => $inscritos,
        ));
    }

    /**
     * Creates a new inscrito entity.
     *
     */
    public function newAction(Request $request)
    {
        $inscrito = new Inscritos();
        $form = $this->createForm('BackendBundle\Form\InscritosType', $inscrito);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($inscrito);
            $em->flush();

            return $this->redirectToRoute('inscritos_show', array('id' => $inscrito->getId()));
        }

        return $this->render('BackendBundle:inscritos:new.html.twig', array(
            'inscrito' => $inscrito,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a inscrito entity.
     *
     */
    public function showAction(Inscritos $inscrito)
    {
        $deleteForm = $this->createDeleteForm($inscrito);

        return $this->render('BackendBundle:inscritos:show.html.twig', array(
            'inscrito' => $inscrito,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing inscrito entity.
     *
     */
    public function editAction(Request $request, Inscritos $inscrito)
    {
        $deleteForm = $this->createDeleteForm($inscrito);
        $editForm = $this->createForm('BackendBundle\Form\InscritosType', $inscrito);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('inscritos_edit', array('id' => $inscrito->getId()));
        }

        return $this->render('BackendBundle:inscritos:edit.html.twig', array(
            'inscrito' => $inscrito,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a inscrito entity.
     *
     */
    public function deleteAction(Request $request, Inscritos $inscrito)
    {
        $form = $this->createDeleteForm($inscrito);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($inscrito);
            $em->flush();
        }

        return $this->redirectToRoute('inscritos_index');
    }

    /**
     * Creates a form to delete a inscrito entity.
     *
     * @param Inscritos $inscrito The inscrito entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Inscritos $inscrito)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('inscritos_delete', array('id' => $inscrito->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
