<?php

namespace BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
Use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class UsersType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                //  ->add('id')
                ->add('role', ChoiceType::class, array('choices' => array(
                        'Administrador' => 'ROLE_ADMIN',
                        'Administrador intermedio' => 'ROLE_MEDIUN_ADMIN',
                        'SUPER Admin' => 'ROLE_SUPER_ADMIN'),
                    'label' => 'Tipo administrador : ',
                    'required' => true,
                    "attr" => array("class" => "form-name form-control")))
                ->add('email', RepeatedType::class, array(
                    "required" => "required",
                    'type' => EmailType::class,
                    'invalid_message' => 'Los dos correos deben coincidir',
                    'first_options' => array('label' => 'Correo electronico', "attr" => array("class" => "form-email form-control")),
                    'second_options' => array('label' => 'Repita el Correo electronico', "attr" => array("class" => "form-email form-control"))
                ))
                ->add('name', TextType::class, array("label" => "Nombre: ",
                    "required" => "required",
                    "attr" => array("class" => "form-control input")))
                ->add('surname', TextType::class, array("label" => "Apellidos: ",
                    "required" => "required",
                    "attr" => array("class" => "form-control input")))

//                ->add('password', TextType::class, array("label" => "Password: ",
//                    "required" => "required",
//                    "attr" => array("class" => "form-password form-control")))
                //->add('nick')
                //->add('bio')
//                ->add("isActive", CheckboxType::class, array(
//                    "label" => "Activo: ",
//                    "required" => "required",
//                    "attr" => array('class' => "form-control css-checkbox"),
//                ))
                // ->add('image')
                ->add('nif', TextType::class, array("label" => "DNI: ",
                    "required" => "required",
                    "attr" => array("class" => "form-name form-control")))
                //->add('nivelUsuario')
                // ->add('nivelAcceso')
                // ->add('fechaUltima')
                // ->add('codigoRec')
                ->add('bloqueo', ChoiceType::class, array('choices' => array(
                        'NO' => 'no',
                        'SI' => 'si'),
                    'label' => '¿Esta bloqueado? : ',
                    'required' => true,
                    "attr" => array("class" => "form-name form-control")))
                ->add('razonBloqueo')
                // ->add('fechaControl')
                // ->add('idCcaa')
                ->add('idProvincia', EntityType::class, array("label" => "Provincia: ",
                    'required' => false,
                    "class" => "BackendBundle:Provincia",
                    'placeholder' => 'Selecciona una provincia',
                    "attr" => array("class" => "form-name form-control class_select_provincia")))
                ->add('idMunicipio', EntityType::class, array("label" => "Municipio: ",
                    'required' => false,
                    "class" => "BackendBundle:Municipios",
                    'placeholder' => 'Primero selecciona una provincia',
                    "attr" => array("class" => "form-name form-control  class_select_municipio")))
                ->add("enviar", CheckboxType::class, array(
                    "label" => "Enviar correo al usuario: ",
                    "required" => false,
                    "mapped" => false,
                    "attr" => array('class' => "form-control css-checkbox", 'checked' => 'checked'),
                ))

        ;

        if ('crear_usuario' === $options['accion']) {
            $builder
                    ->add('password', TextType::class, array("label" => "Password: ",
                    "required" => "required",
                    "attr" => array("class" => "form-password form-control")))
            ;
        } elseif ('modificar_perfil' === $options['accion']) {
//            $builder
//                    ->add('guardar', 'submit', array(
//                        'label' => 'Guardar cambios',
//                    ))
//            ;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'BackendBundle\Entity\Users',
            'accion' => 'modificar_perfil',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'backendbundle_users';
    }

}
