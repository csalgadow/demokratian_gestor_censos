<?php

namespace BackendBundle\Controller;

use BackendBundle\Entity\Inscritos;
use BackendBundle\Entity\InscritosAcciones;
use BackendBundle\Entity\ValidaInscritos;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Inscrito controller.
 *
 */
class InscritosController extends Controller {

    private $session;

    public function __construct() {
        $this->session = new Session();
    }

    /**
     * Lists all inscrito entities.
     *
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();


        $dql = "SELECT e FROM BackendBundle:Inscritos e";
        $query = $em->createQuery($dql);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, $request->query->getInt('page', 1), 15
        );

        return $this->render('BackendBundle:inscritos:index.html.twig', array(
                    'title' => "Listado de todos los inscritos",
                    'type' => "1",
                    'pagination' => $pagination
        ));
    }

    /**
     * Lista todos los inscritos pendientes de validar.
     *
     */
/*
    public function inscritosPendientesAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $idUser = $this->getUser()->getId();


//        $dql = "SELECT e FROM BackendBundle:Inscritos e, BackendBundle:InscritosAcciones p where e.id = p.inscritosId and p.status!=2";
//        $query = $em->createQuery($dql);

//        $query = $em->createQueryBuilder()
//                ->from("BackendBundle:Inscritos", "e")
//                ->select("e")
//                ->innerJoin("BackendBundle:InscritosAcciones", "u", "WITH", "e.id=u.inscritosId")
//                //->innerJoin("BackendBundle:ValidaInscritos", "c", "WITH", "e.id=c.inscritos")
////                ->innerJoin("BackendBundle:InscritosAcciones", "u")
////                ->innerJoin("BackendBundle:ValidaInscritos", "c")
////                ->andWhere ("e.id=u.inscritosId")
////                ->andWhere ("e.id=c.inscritos")
//                //  ->distinct("u.status")
//                ->distinct("e.inscritosAcciones")
//                //->groupBy("u.inscritosId")
//                // ->andWhere('c.inscritos != :null')->setParameter('null', serialize(null)) //not null
//                ->andWhere("u.status != 2")
//                // ->groupBy('e.id')
//                // ->andWhere("u. acciones!= 2")
//                //  ->andWhere("c.users != :idUser")->setParameter('idUser', $idUser)
//                //->distinct("u.inscritosId")
//                // ->groupBy('c.users')
//                //->addOrderBy("COALESCE( u.timeCreated, p.timeCreated )", "DESC")
//                // ->setMaxResults(28)
//                ->getQuery();
//$pagination = $query->getResult();

        $qb = $em->createQueryBuilder();

        $nots = $qb->select('u')
                ->from('BackendBundle:InscritosAcciones', 'u')
                ->where($qb->expr()->eq('u.status', 2))
                ->getQuery()
                ->getResult();

        $query = $qb->select('e')
                ->from('BackendBundle:Inscritos', 'e')
                ->where($qb->expr()->notIn('e.id', $nots->getValidaInscritos()))
                ->getQuery()
                ->getResult();


        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, $request->query->getInt('page', 1), 15
        );
        //dump($pagination);
        return $this->render('BackendBundle:inscritos:index.html.twig', array(
                    'title' => "Listado de los inscritos pendientes de validación",
                    'type' => "3",
                    'pagination' => $pagination
        ));
    }
*/

    public function inscritosPendientesAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
                ->from("BackendBundle:Inscritos", "e")
                ->select("e")
                 ->innerJoin("BackendBundle:InscritosAcciones", "u", "WITH", "e.id=u.inscritosId")
                 ->groupBy("u.status")
                ->where("u.status != 2")

                ->getQuery();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, $request->query->getInt('page', 1), 15

        );

        return $this->render('BackendBundle:inscritos:index.html.twig', array(
                    'title' => "Listado de los inscritos pendientes de validación",
                    'type' => "3",
                    'pagination' => $pagination
        ));
    }


    /**
     * Lists all inscritos  validados.
     *
     */
    public function inscritosValidadosAction(Request $request) {
        $em = $this->getDoctrine()->getManager();




//        $dql = "SELECT e FROM BackendBundle:Inscritos e, BackendBundle:InscritosAcciones p where e.id = p.inscritosId and p.status=2";
//        $dql = "SELECT e FROM BackendBundle:Inscritos e, BackendBundle:InscritosAcciones p where e.id = p.inscritosId and p.status=2";
//        $query = $em->createQuery($dql);

        $query = $em->createQueryBuilder()
                ->from("BackendBundle:Inscritos", "e")
                ->select("e")
                ->leftJoin("BackendBundle:InscritosAcciones", "u", "WITH", "e.id=u.inscritosId")
                //->leftJoin("Product", "p", "WITH", "e.pid=p.id")
                ->where("u.status = 2")
                //->groupBy('u.status')
                //->addOrderBy("COALESCE( u.timeCreated, p.timeCreated )", "DESC")
                // ->setMaxResults(28)
                ->getQuery();
//$pagination = $query->getResult();



        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, $request->query->getInt('page', 1), 15
        );

        return $this->render('BackendBundle:inscritos:index.html.twig', array(
                    'title' => "Listado de los inscritos validados",
                    'type' => "2",
                    'pagination' => $pagination
        ));
    }

    public function searchAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $search = $request->query->get('search', null);

        if ($search == null) {
            return $this->redirectToRoute('inscritos_index');
        }

        $dql = "SELECT e FROM BackendBundle:Inscritos e WHERE e.nombreUsuario LIKE :search OR e.apellidoUsuario LIKE :search OR e.correoUsuario LIKE :search OR e.documento LIKE :search";
        $query = $em->createQuery($dql)->setParameter('search', "%$search%");


        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, $request->query->getInt('page', 1), 2
        );

        return $this->render('BackendBundle:inscritos:index.html.twig', array(
                    'title' => "Listado de la busqueda realizada " . $search . " ",
                    'pagination' => $pagination
        ));
    }

    /**
     * Creates a new inscrito entity.
     *
     */
    public function newAction(Request $request) {
        $inscrito = new Inscritos();
        $form = $this->createForm('BackendBundle\Form\InscritosType', $inscrito);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            dump($errors);
            // metemos los datos en la base de datos que se generan automaticamnete y que el usuario no mete en el formulario
            $fechaInscr = new \DateTime("now");
            $inscrito->setFechainscritpcion($fechaInscr);
            $ip = $request->getClientIp();
            $inscrito->setIpinscripcion($ip);
            $cadenaSeg = "cadena de seguridad!";
            $inscrito->setCadenaseg($cadenaSeg);
            $inscrito->setRazonBloqueo("");
            //$inscrito->setIdProvincia('19');

            $inscrito->setIdMunicipio($form->get("municipioInscritos")->getData());
            $inscrito->setIdProvincia($form->get("provinciaInscritos")->getData());

            $em->persist($inscrito);
            $em->flush();

            return $this->redirectToRoute('inscritos_show', array('id' => $inscrito->getId()));
        }

        return $this->render('BackendBundle:inscritos:new.html.twig', array(
                    'inscrito' => $inscrito,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a inscrito entity.
     *
     */
    public function showAction(Inscritos $inscrito) {
        $deleteForm = $this->createDeleteForm($inscrito);

        return $this->render('BackendBundle:inscritos:show.html.twig', array(
                    'inscrito' => $inscrito,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing inscrito entity.
     *
     */
    public function editAction(Request $request, Inscritos $inscrito) {
        $deleteForm = $this->createDeleteForm($inscrito);
        $editForm = $this->createForm('BackendBundle\Form\InscritosType', $inscrito);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('inscritos_edit', array('id' => $inscrito->getId()));
        }

        return $this->render('BackendBundle:inscritos:edit.html.twig', array(
                    'inscrito' => $inscrito,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a inscrito entity.
     *
     */
    public function deleteAction(Request $request, Inscritos $inscrito) {
        $form = $this->createDeleteForm($inscrito);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($inscrito);
            $em->flush();
        }

        return $this->redirectToRoute('inscritos_index');
    }

    /**
     * Creates a form to delete a inscrito entity.
     *
     * @param Inscritos $inscrito The inscrito entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Inscritos $inscrito) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('inscritos_delete', array('id' => $inscrito->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    public function municipioTestAction(Request $request) {
        $provincia_id = $request->get('provincia_id');


        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery('SELECT u.nombre, u.idMunicipio FROM BackendBundle:Municipios u WHERE u.idProvincia = :id_provincia')
                ->setParameter('id_provincia', $provincia_id);

        $provincia = $query->getResult();

        return new JsonResponse($provincia);
    }

}
