<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
//use BackendBundle\Entity\Entries;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $indexCategory = 1; //categoria de la pagina de inicio
        $indexSlider = 9;
        $em = $this->getDoctrine()->getManager();

        $entry_repo = $em->getRepository("BackendBundle:Entries");
        $entries = $entry_repo->findBy(
                array('category' => $indexCategory), array('id' => 'ASC')
        );
        $sliders = $entry_repo->findBy(
                array('category' => $indexSlider), array('id' => 'ASC')
        );

        return $this->render('AppBundle:Default:index.html.twig', array(
                    "entries" => $entries,
                    "sliders" => $sliders
        ));
    }
}
