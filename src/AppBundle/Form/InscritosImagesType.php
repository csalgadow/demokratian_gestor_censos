<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class InscritosImagesType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
        // ->add('id')
        // ->add('idInscrito', TextType::class, array("label" => "imagen añadidad: ",
        //     "required" => "required",
        //     "attr" => array("class" => "form-name form-control input")))               
        // ->add('fecha')
        // ->add('inscritos')
               ->add('images', FileType::class, array("label" => "Incluir un archivo solicitado: ",
                    "attr" => array("class" => "form-name form-control unidades"),
                    "data_class" => null,
                   "required" => false,
                  //  'multiple' => true
                ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'BackendBundle\Entity\InscritosImages'
        ));
        $resolver->setDefaults(array(
            'allow_extra_fields' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'backendbundle_inscritosimages';
    }

}
