<?php

namespace BackendBundle\Controller;

use BackendBundle\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use BackendBundle\Form\Users2Type;
//use BackendBundle\Form\User2Type;
use BackendBundle\Form\UsersPassType;
use BackendBundle\Form\ChangePasswordType;
use BackendBundle\Form\Model\ChangePassword;

/**
 * Others controller.
 *
 */
class OthersController extends Controller {

    private $session;

    public function __construct() {
        $this->session = new Session();
    }

    /**
     * Help page.
     *
     */
    public function helpAction(Request $request) {

        return $this->render('BackendBundle:others:help.html.twig');
    }
    
    /**
     * Help page.
     *
     */
    public function descargarAction(Request $request) {

        return $this->render('BackendBundle:others:descargas.html.twig');
    }

    /**
     * Creates a new user entity.
     *
     */
    public function newAction(Request $request) {
        
    }

    /**
     * Finds and displays a user entity.
     *
     */
    public function showAction() {
        $userId = $this->getUser(); //metemos como id la del usuario sacada de su sesion

        $em = $this->getDoctrine()->getManager();
        $user_repo = $em->getRepository("BackendBundle:Users");
        $user = $user_repo->find($userId);



        return $this->render('BackendBundle:others:show.html.twig', array(
                    'user' => $user,
        ));
    }

    /**
     * Displays a form to edit a user entity.
     *
     */
    public function editAction(Request $request) {

        $userId = $this->getUser(); //metemos como id la del usuario sacada de su sesion

        $em = $this->getDoctrine()->getManager();
        $user_repo = $em->getRepository("BackendBundle:Users");
        $user = $user_repo->find($userId);
        //$user_email = $user->getEmail();  //cogemos el dato del email

        $form = $this->createForm(Users2Type::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                $em->persist($user);
                $flush = $em->flush();
                if ($flush === null) {
                    $this->session->getFlashBag()->add('success', 'El usuario ' . $form->get("email")->getData() . ' se ha editado correctamente');
                    //return $this->redirectToRoute("demokratian_index_user"); //redirigimos la pagina si se incluido correctamete
                } else {
                    $this->session->getFlashBag()->add('warning', 'Error al editar El Usuario');
                }
            } else {
                $this->session->getFlashBag()->add('warning', 'El usuario no se ha editado por un error en el formulario !');
            }
        }

        return $this->render("BackendBundle:Others:edit.html.twig", array(
                    'user' => $user,
                    "form" => $form->createView()));
    }


    public function passAction(Request $request) {
        $changePasswordModel = new ChangePassword();
        $form = $this->createForm(ChangePasswordType::class, $changePasswordModel);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $user = $this->getUser(); //metemos como id la del usuario sacado de su sesion
                $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
                $password = $encoder->encodePassword($changePasswordModel->getPassword(), $user->getSalt());
                $user->setPassword($password);
                $em->persist($user);
                $flush = $em->flush();
                if ($flush === null) {
                    $this->session->getFlashBag()->add('success', 'El usuario se ha editado correctamente');
                    return $this->redirectToRoute("others_show"); //redirigimos la pagina si se incluido correctamete
                } else {
                    $this->session->getFlashBag()->add('warning', 'Error al editar el password');
                }
            } else {
                dump($form->getErrors());
                $this->session->getFlashBag()->add('warning', 'El password no se ha editado por un error en el formulario !');
            }
        }

        return $this->render('BackendBundle:Others:editPass.html.twig', array(
                    'form' => $form->createView(),
        ));
    }


}
