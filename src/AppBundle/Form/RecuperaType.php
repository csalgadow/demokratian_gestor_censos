<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
//use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use AppBundle\Form\Listener\AddStateFieldSubscriber;

class RecuperaType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                /*->add('nombreUsuario', TextType::class, array("label" => "Nombre: ",
                    "required" => "required",
                    "attr" => array("class" => "form-control input")))
                ->add('apellidoUsuario', TextType::class, array("label" => "Apellidos: ",
                    "required" => "required",
                    "attr" => array("class" => "form-name form-control input")))
                    */
                ->add('correoUsuario', RepeatedType::class, array(
                    "required" => "required",
                    'type' => EmailType::class,
                    'invalid_message' => 'Los dos correos deben coincidir',
                    'first_options' => array('label' => 'Correo electronico', "attr" => array("class" => "form-email form-control")),
                    'second_options' => array('label' => 'Repita el Correo electronico', "attr" => array("class" => "form-email form-control"))
                ))
                ->add('tipodocumento', ChoiceType::class, array("label" => "Tipo de documento: ",
                    "choices" => array(
                        "DNI" => "DNI",
                        "NIE" => "NIE",
                        "Pasaporte" => "pasaporte"),
                    "attr" => array("class" => "form-name form-control")))
                ->add('documento', TextType::class, array("label" => "Documento: ",
                    "required" => "required",
                    "attr" => array("class" => "form-name form-control")))
                /*->add('direccion', HiddenType::class, [
                        'data' => 'abcdef',])
                ->add('codigopostal', HiddenType::class, [
                      'data' => 'abcdef',])
                ->add('fechanacimiento', HiddenType::class, [
                      'data' => '2018-01-01',])*/
        ;

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
      //  $resolver->setDefaults(array(
      //      'data_class' => 'BackendBundle\Entity\Inscritos'
      //  ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'backendbundle_inscritos';
    }

}
