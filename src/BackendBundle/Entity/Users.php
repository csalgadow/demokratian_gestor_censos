<?php

namespace BackendBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;

/**
 * Users
 * @UniqueEntity(fields={"email"}, message="¡Este correo ya esta registrado!")
 */
class Users implements UserInterface {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $role;

    /**
     * @var string
     * @Assert\Email(checkMX=true)
     */
    private $email;

    /**
     * @var string
     * @Assert\NotBlank(message = "Por favor, escribe el nombre")
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank(message = "Por favor, escribe los  apellidos")
     */
    private $surname;

    /**
     * @var string
     * @Assert\NotBlank(message = "No puedes dejar el password vacio")
     * @Assert\Length(min = 6)
     */
    private $password;

    /**
     * @var string
     */
    private $nick;

    /**
     * @var string
     */
    private $bio;

    /**
     * @var boolean
     */
    private $isActive = '0';

    /**
     * @var string
     */
    private $image;

    /**
     * @var string
     * @Assert\NotBlank(message = "Por favor, escribe El DNI o documento")
     */
    private $nif;

    /**
     * @var integer
     */
    private $nivelUsuario = '1';

    /**
     * @var integer
     */
    private $nivelAcceso = '10';

    /**
     * @var \DateTime
     */
    private $fechaUltima;

    /**
     * @var string
     */
    private $codigoRec;

    /**
     * @var string
     */
    private $bloqueo = 'no';

    /**
     * @var string
     */
    private $razonBloqueo;

    /**
     * @var \DateTime
     */
    private $fechaControl;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $entry;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $usuarioValida;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $preferenciasUser;

    /**
     * @var \BackendBundle\Entity\Ccaa
     */
    private $idCcaa;

    /**
     * @var \BackendBundle\Entity\Provincia
     * @Assert\NotBlank(message = "Por favor,  indica la provincia")
     */
    private $idProvincia;

    /**
     * @var \BackendBundle\Entity\Municipios
     * @Assert\NotBlank(message = "Por favor, indica el municipio")
     */
    private $idMunicipio;

    /**
     * Constructor
     */
    public function __construct() {
        $this->entry = new \Doctrine\Common\Collections\ArrayCollection();
        $this->usuarioValida = new \Doctrine\Common\Collections\ArrayCollection();
        $this->preferenciasUser = new \Doctrine\Common\Collections\ArrayCollection();

        $this->isActive = true;
        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid('', true));
    }

    public function __toString() {
        // return strval($this->idCcaa);
        //return $this->idCcaa;
    }

    //auth
    public function getUsername() {
        return $this->email;
    }

    public function getSalt() {
        return null;
    }

    public function getRoles() {
        return array($this->getRole());
    }

    public function eraseCredentials() {
        
    }

    //end auth

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Users
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return Users
     */
    public function setRole($role) {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole() {
        return $this->role;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Users
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Users
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return Users
     */
    public function setSurname($surname) {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname() {
        return $this->surname;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Users
     */
    public function setPassword($password) {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * Set nick
     *
     * @param string $nick
     *
     * @return Users
     */
    public function setNick($nick) {
        $this->nick = $nick;

        return $this;
    }

    /**
     * Get nick
     *
     * @return string
     */
    public function getNick() {
        return $this->nick;
    }

    /**
     * Set bio
     *
     * @param string $bio
     *
     * @return Users
     */
    public function setBio($bio) {
        $this->bio = $bio;

        return $this;
    }

    /**
     * Get bio
     *
     * @return string
     */
    public function getBio() {
        return $this->bio;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Users
     */
    public function setIsActive($isActive) {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive() {
        return $this->isActive;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Users
     */
    public function setImage($image) {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * Set nif
     *
     * @param string $nif
     *
     * @return Users
     */
    public function setNif($nif) {
        $this->nif = $nif;

        return $this;
    }

    /**
     * Get nif
     *
     * @return string
     */
    public function getNif() {
        return $this->nif;
    }

    /**
     * Set nivelUsuario
     *
     * @param integer $nivelUsuario
     *
     * @return Users
     */
    public function setNivelUsuario($nivelUsuario) {
        $this->nivelUsuario = $nivelUsuario;

        return $this;
    }

    /**
     * Get nivelUsuario
     *
     * @return integer
     */
    public function getNivelUsuario() {
        return $this->nivelUsuario;
    }

    /**
     * Set nivelAcceso
     *
     * @param integer $nivelAcceso
     *
     * @return Users
     */
    public function setNivelAcceso($nivelAcceso) {
        $this->nivelAcceso = $nivelAcceso;

        return $this;
    }

    /**
     * Get nivelAcceso
     *
     * @return integer
     */
    public function getNivelAcceso() {
        return $this->nivelAcceso;
    }

    /**
     * Set fechaUltima
     *
     * @param \DateTime $fechaUltima
     *
     * @return Users
     */
    public function setFechaUltima($fechaUltima) {
        $this->fechaUltima = $fechaUltima;

        return $this;
    }

    /**
     * Get fechaUltima
     *
     * @return \DateTime
     */
    public function getFechaUltima() {
        return $this->fechaUltima;
    }

    /**
     * Set codigoRec
     *
     * @param string $codigoRec
     *
     * @return Users
     */
    public function setCodigoRec($codigoRec) {
        $this->codigoRec = $codigoRec;

        return $this;
    }

    /**
     * Get codigoRec
     *
     * @return string
     */
    public function getCodigoRec() {
        return $this->codigoRec;
    }

    /**
     * Set bloqueo
     *
     * @param string $bloqueo
     *
     * @return Users
     */
    public function setBloqueo($bloqueo) {
        $this->bloqueo = $bloqueo;

        return $this;
    }

    /**
     * Get bloqueo
     *
     * @return string
     */
    public function getBloqueo() {
        return $this->bloqueo;
    }

    /**
     * Set razonBloqueo
     *
     * @param string $razonBloqueo
     *
     * @return Users
     */
    public function setRazonBloqueo($razonBloqueo) {
        $this->razonBloqueo = $razonBloqueo;

        return $this;
    }

    /**
     * Get razonBloqueo
     *
     * @return string
     */
    public function getRazonBloqueo() {
        return $this->razonBloqueo;
    }

    /**
     * Set fechaControl
     *
     * @param \DateTime $fechaControl
     *
     * @return Users
     */
    public function setFechaControl($fechaControl) {
        $this->fechaControl = $fechaControl;

        return $this;
    }

    /**
     * Get fechaControl
     *
     * @return \DateTime
     */
    public function getFechaControl() {
        return $this->fechaControl;
    }

    /**
     * Add entry
     *
     * @param \BackendBundle\Entity\Entries $entry
     *
     * @return Users
     */
    public function addEntry(\BackendBundle\Entity\Entries $entry) {
        $this->entry[] = $entry;

        return $this;
    }

    /**
     * Remove entry
     *
     * @param \BackendBundle\Entity\Entries $entry
     */
    public function removeEntry(\BackendBundle\Entity\Entries $entry) {
        $this->entry->removeElement($entry);
    }

    /**
     * Get entry
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEntry() {
        return $this->entry;
    }

    /**
     * Add usuarioValida
     *
     * @param \BackendBundle\Entity\ValidaInscritos $usuarioValida
     *
     * @return Users
     */
    public function addUsuarioValida(\BackendBundle\Entity\ValidaInscritos $usuarioValida) {
        $this->usuarioValida[] = $usuarioValida;

        return $this;
    }

    /**
     * Remove usuarioValida
     *
     * @param \BackendBundle\Entity\ValidaInscritos $usuarioValida
     */
    public function removeUsuarioValida(\BackendBundle\Entity\ValidaInscritos $usuarioValida) {
        $this->usuarioValida->removeElement($usuarioValida);
    }

    /**
     * Get usuarioValida
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsuarioValida() {
        return $this->usuarioValida;
    }

    /**
     * Add preferenciasUser
     *
     * @param \BackendBundle\Entity\Preferencias $preferenciasUser
     *
     * @return Users
     */
    public function addPreferenciasUser(\BackendBundle\Entity\Preferencias $preferenciasUser) {
        $this->preferenciasUser[] = $preferenciasUser;

        return $this;
    }

    /**
     * Remove preferenciasUser
     *
     * @param \BackendBundle\Entity\Preferencias $preferenciasUser
     */
    public function removePreferenciasUser(\BackendBundle\Entity\Preferencias $preferenciasUser) {
        $this->preferenciasUser->removeElement($preferenciasUser);
    }

    /**
     * Get preferenciasUser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPreferenciasUser() {
        return $this->preferenciasUser;
    }

    /**
     * Set idCcaa
     *
     * @param \BackendBundle\Entity\Ccaa $idCcaa
     *
     * @return Users
     */
    public function setIdCcaa(\BackendBundle\Entity\Ccaa $idCcaa = null) {
        $this->idCcaa = $idCcaa;

        return $this;
    }

    /**
     * Get idCcaa
     *
     * @return \BackendBundle\Entity\Ccaa
     */
    public function getIdCcaa() {
        return $this->idCcaa;
    }

    /**
     * Set idMunicipio
     *
     * @param \BackendBundle\Entity\Municipios $idMunicipio
     *
     * @return Users
     */
    public function setIdMunicipio(\BackendBundle\Entity\Municipios $idMunicipio = null) {
        $this->idMunicipio = $idMunicipio;

        return $this;
    }

    /**
     * Get idMunicipio
     *
     * @return \BackendBundle\Entity\Municipios
     */
    public function getIdMunicipio() {
        return $this->idMunicipio;
    }

    /**
     * Set idProvincia
     *
     * @param \BackendBundle\Entity\Provincia $idProvincia
     *
     * @return Users
     */
    public function setIdProvincia(\BackendBundle\Entity\Provincia $idProvincia = null) {
        $this->idProvincia = $idProvincia;

        return $this;
    }

    /**
     * Get idProvincia
     *
     * @return \BackendBundle\Entity\Provincia
     */
    public function getIdProvincia() {
        return $this->idProvincia;
    }

    /** @see \Serializable::serialize() */
    public function serialize() {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
                // see section on salt below
                // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized) {
        list (
                $this->id,
                $this->username,
                $this->password,
                // see section on salt below
                // $this->salt
                ) = unserialize($serialized);
    }

}
