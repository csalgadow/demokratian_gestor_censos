<?php

namespace AppBundle\Controller;

use BackendBundle\Entity\InscritosImages;
use BackendBundle\Entity\Inscritos;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Repository\security_encript;

/**
 * Inscritosimage controller.
 *
 */
class InscritosImagesController extends Controller {

    private $session;

    public function __construct() {
        $this->session = new Session();
    }

    /**
     * Creates a new inscritosImage entity.
     *
     */
    public function newAction(Request $request, $valida) {
        $inscritosImages = new Inscritosimages();
        $form = $this->createForm('AppBundle\Form\InscritosImagesType', $inscritosImages);
        $form->handleRequest($request);

        $indexRegistro = 5;
        $em = $this->getDoctrine()->getManager();

        $entry_repo = $em->getRepository("BackendBundle:Entries");
        $entries = $entry_repo->findBy(
                array('category' => $indexRegistro), array('id' => 'ASC')
        );

        if ($form->isSubmitted() && $form->isValid()) {
            // if ($this->captchaverify($request->get('g-recaptcha-response'))) {

            $postVal = $request->request->get('val');
            $digControl = substr($postVal, 0, 4);

            //$datoId= $form->get("valida")->getData();
            //echo $datoId;
            //  $data = $form->getData();
            //$datoVal = $form->$request->request->get($form->getName());
            //$datoVal = $form->get("val")->getData();
            //echo $datoVal;
            //$postData = $request->request->get('backendbundle_inscritosimages');
            //$valId = $postData['inscritos'];
            //dump($form);
            //       exit;
            if ($digControl == "DKp2") {
                $em = $this->getDoctrine()->getManager();

                $encripta = new security_encript();
                $clave = $this->getParameter('secret');
                $rest = substr($postVal, 4);
                $cadena = $encripta->decrypt($rest, $clave);
                $subCadena = explode("__", $cadena);

                if (empty($subCadena[0])){
                  $this->session->getFlashBag()->add('warning', ' Hay un error!!, si ha modificado algo manualmente el sistema no funcionara,  si no, contacte con el administrador del sitio');
                }else{
                $laClave = $subCadena[0];
                $correo = $subCadena[1];
                $ident = $subCadena[2];



                $inscritos = $em->getRepository("BackendBundle:Inscritos");
                $inscrito = $inscritos->findOneBy(
                        array('id' => $ident,
                    'correoUsuario' => $correo,
                    'cadenaseg' => $laClave), array('id' => 'ASC')
                );

                $idInscrito = $inscrito->getId();
                  if (!empty ($inscrito)){
                //if (count($inscrito) == 1) {
                    $status = false;
                    $file = $form["images"]->getData(); //cogemos los datos de la imagen
                    //$images = $request->request->get('images'); //cogemos los datos
                    if ($file != null) {

                        $ext = $file->guessExtension(); //miramos la extension
                        $file_name = $file->getClientOriginalName();
                        $file_size = $file->getClientSize(); //miramos el tamaño
                        $file_type = $file->getMimeType(); // miramos el tipo de archivo que es para comprobar que es una imagen
                        $size_file = $this->getParameter('security_size_file');
                        $maxsize = $size_file * 1024;
                        if ($file_size >= $maxsize) {  //miramos a ver si excede el tamaño permitido
                            $file_size = ceil($file_size / 1024);
                            $this->session->getFlashBag()->add('warning', 'La imagen es demasiado grande. El tamaño maximo es de : ' . $size_file . ' kb y su archivo tiene: ' . $file_size . ' kb');
                        } else {

                            if ($file_type == "image/jpg" || $file_type == "image/jpeg" || $file_type == "image/png" || $file_type == "image/gif") {

                                $t = 0;
                                $nombre_base = basename($file_name, $ext); //quitamos la extension
                                $nombre_base = substr($nombre_base, 0, -1); //quitamos el punto que se queda
                                $nombre_base = htmlentities(str_replace(' ', '_', (trim($nombre_base))));
                                $nombre_base = $idInscrito . '_' . $nombre_base; //quitamos la extension
                                //$carpeta= str_replace(' ','_',$ruta);
                                $new_file_name = $nombre_base . "." . $ext;
                                while (file_exists($this->getParameter('upload_file') . $new_file_name)) {

                                    $actual_name = $nombre_base . "_" . $t;
                                    $new_file_name = $actual_name . "." . $ext;
                                    $t++;
                                }

                                $file->move($this->getParameter('security_upload_file'), $new_file_name); //subimos el archivo a la carpeta que tenemos definida en config.yml como upload_file que estara dentro de la carpeta web

                                $status = "ok";  //avisamos que la imagen se ha subido
                            } else {
                                $this->session->getFlashBag()->add('warning', 'El archivo enviado no es una imagen, es del tipo: ' . $file_type);
                            }
                        }

                        if ($status == "ok") {
                            $fecha = new \DateTime("now");
                            $inscritosImages->setFecha($fecha);
                            $inscritosImages->setInscritos($inscrito);

                            $inscritosImages->setImages($new_file_name);

                            $em->persist($inscritosImages);
                            $flush = $em->flush();
                            if ($flush == null) {
                                $this->session->getFlashBag()->add('success', 'Se ha añadido correctamente el archivo: ' . $file_name . ' Puede añadir otro archivo si lo desea');
                            }
                        }
                        /*  foreach ($images as $laImagen) {
                          //$file = $this->getRequest()->files->get('images');
                          $fecha = new \DateTime("now");
                          $inscritosImages->setFecha($fecha);
                          $inscritosImages->setInscritos($inscrito);

                          echo $laImagen;
                          echo "<br/>";
                          $inscritosImages->setImages($laImagen);

                          $em->persist($inscritosImages);
                          $em->flush();
                          //$em->clear();
                          $i++;
                          } */
                    } else {
                        $this->session->getFlashBag()->add('warning', ' No ha seleccionado ningun archivo');
                    }


                    //  return $this->redirectToRoute('inscritosimages_show', array('id' => $inscritosImages->getId()));
                } else {
                    //hay un error
                    $this->session->getFlashBag()->add('warning', ' Hay un error!!, si ha modificado algo manualmente el sistema no funcionara,  si no, contacte con el administrador del sitio');
                }
              }
            } else {
                $this->session->getFlashBag()->add('warning', ' Hay un error!!, si ha modificado algo manualmente el sistema no funcionara,  si no, contacte con el administrador del sitio');
            }
            //}
        }
        return $this->render('AppBundle:inscritos:restoDocu.html.twig', array(
                    // 'inscritosImages' => $inscritosImages,
                    'valida' => $valida,
                    "entries" => $entries,
                    'form' => $form->createView(),
        ));
    }

    public function finalizarAction($valida) {
        $inscritosAcciones = new \BackendBundle\Entity\InscritosAcciones();



        $digControl = substr($valida, 0, 4);
        $em = $this->getDoctrine()->getManager();

        if ($digControl == "DKp2") {

            $encripta = new security_encript();
            $clave = $this->getParameter('secret');
            $rest = substr($valida, 4);
            $cadena = $encripta->decrypt($rest, $clave);
            $subCadena = explode("__", $cadena);
            if (empty($subCadena[0])){
              $this->session->getFlashBag()->add('warning', ' Hay un error!!, si ha modificado algo manualmente el sistema no funcionara,  si no, contacte con el administrador del sitio');
            }else{
            $laClave = $subCadena[0];
            $correo = $subCadena[1];
            $ident = $subCadena[2];



            $inscritos = $em->getRepository("BackendBundle:Inscritos");
            $inscrito = $inscritos->findOneBy(
                    array('id' => $ident,
                'correoUsuario' => $correo,
                'cadenaseg' => $laClave), array('id' => 'ASC')
            );


            if (!empty ($inscrito)){
            //if (count($inscrito) == 1) {
                //es correcto
                $tipo = 1;
                $acciones = $em->getRepository("BackendBundle:Acciones");
                $accion = $acciones->findOneBy(
                        array('id' => $tipo)
                );

                $inscritosAcciones->setAcciones($accion);

                $inscritosAcciones->setInscritos($inscrito);
                $fecha = new \DateTime("now");
                $inscritosAcciones->setFecha($fecha);
                $inscritosAcciones->setAccion("Incluido por el usuario   el sistema");
                $em->persist($inscritosAcciones);
                $flush = $em->flush();
                if ($flush == null) {


                    $this->session->getFlashBag()->add('success', ' Se ha terminado correctamente!!');
                } else {
                    $this->session->getFlashBag()->add('warning', ' Hay un error con el acceso a la base de datos!! contacte con el administrador del sitio');
                }
            } else {
                //hay un error
                $this->session->getFlashBag()->add('warning', ' Hay un error!!, si ha modificado algo manualmente el sistema no funcionara,  si no, contacte con el administrador del sitio');
            }
          }
        } else {
            $this->session->getFlashBag()->add('warning', ' Hay un error!!, si ha modificado algo manualmente el sistema no funcionara,  si no, contacte con el administrador del sitio');
        }

        return $this->render('AppBundle:inscritos:finaliza.html.twig', array(
                    "inscritosAcciones" => $inscritosAcciones,
        ));
    }

}
