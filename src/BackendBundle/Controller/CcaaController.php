<?php

namespace BackendBundle\Controller;

use BackendBundle\Entity\Ccaa;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Ccaa controller.
 *
 */
class CcaaController extends Controller
{
    /**
     * Lists all ccaa entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $ccaas = $em->getRepository('BackendBundle:Ccaa')->findAll();

        return $this->render('BackendBundle:ccaa:index.html.twig', array(
            'ccaas' => $ccaas,
        ));
    }

    /**
     * Creates a new ccaa entity.
     *
     */
    public function newAction(Request $request)
    {
        $ccaa = new Ccaa();
        $form = $this->createForm('BackendBundle\Form\CcaaType', $ccaa);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ccaa);
            $em->flush();

            return $this->redirectToRoute('ccaa_show', array('id' => $ccaa->getId()));
        }

        return $this->render('BackendBundle:ccaa:new.html.twig', array(
            'ccaa' => $ccaa,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ccaa entity.
     *
     */
    public function showAction(Ccaa $ccaa)
    {
        $deleteForm = $this->createDeleteForm($ccaa);

        return $this->render('BackendBundle:ccaa:show.html.twig', array(
            'ccaa' => $ccaa,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ccaa entity.
     *
     */
    public function editAction(Request $request, Ccaa $ccaa)
    {
        $deleteForm = $this->createDeleteForm($ccaa);
        $editForm = $this->createForm('BackendBundle\Form\CcaaType', $ccaa);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('ccaa_edit', array('id' => $ccaa->getId()));
        }

        return $this->render('BackendBundle:ccaa:edit.html.twig', array(
            'ccaa' => $ccaa,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a ccaa entity.
     *
     */
    public function deleteAction(Request $request, Ccaa $ccaa)
    {
        $form = $this->createDeleteForm($ccaa);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($ccaa);
            $em->flush();
        }

        return $this->redirectToRoute('ccaa_index');
    }

    /**
     * Creates a form to delete a ccaa entity.
     *
     * @param Ccaa $ccaa The ccaa entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Ccaa $ccaa)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ccaa_delete', array('id' => $ccaa->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
