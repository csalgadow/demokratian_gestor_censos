<?php

namespace AppBundle\Controller;

use BackendBundle\Entity\Inscritos;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Repository\security_encript;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Inscrito controller.
 *
 */
class InscritosController extends Controller {

    private $session;

    public function __construct() {
        $this->session = new Session();
    }

    /**
     * Creates a new inscrito entity.
     *
     */
    public function newAction(Request $request, \Swift_Mailer $mailer) {
        $inscrito = new Inscritos();
        $form = $this->createForm('AppBundle\Form\InscritosType', $inscrito);

        $indexRegistro = 3;
        $em = $this->getDoctrine()->getManager();

        $entry_repo = $em->getRepository("BackendBundle:Entries");
        $entries = $entry_repo->findBy(
                array('category' => $indexRegistro), array('id' => 'ASC')
        );

        $form->handleRequest($request);

        $data = $form->getData();


        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if ($this->captchaverify($request->get('g-recaptcha-response'))) {

                    if ($form->get("acepto")->getData() == "") {
                        $this->session->getFlashBag()->add('warning', 'No ha marcado la casilla de aceptacion de los terminos legales !');
                    } else {

                        $documento = str_replace(' ', '', $form->get("documento")->getData()); // quitamos los espacios en blanco
                        $documento = str_replace('-', '', $documento); // quitamos si han puesto un guion entre los numeros y la letra

                        $query = $em->createQuery('SELECT u FROM BackendBundle:Inscritos u WHERE u.correoUsuario = :email OR u.documento = :doc')
                                ->setParameter('email', $form->get("correoUsuario")->getData())
                                ->setParameter('doc', $documento);

                        $inscrito_isset = $query->getResult();
                        if (count($inscrito_isset) == 0) {
                            // comprobamos que los datos dependiendo del pais esten completos
                            $elpais = $form->get("pais")->getData();

                            if ($elpais == "ES") {
                                $provincia = $form->get("provinciaInscritos")->getData();
                                $municipio = $form->get("municipioInscritos")->getData();

                                if ($provincia == "" OR $municipio == "") {
                                    $situacion = "ERROR";
                                    $this->session->getFlashBag()->add('warning', 'Fatan datos de provincia o municipio!');
                                } else {
                                    // si es españa y estan completos los datos de provincia y municipio, ponemos a nul los otros datos por si los hubieran completado por error y busccamos la ccaa
                                    $provinciaotros = null;
                                    $municipiootros = null;


                                    $ccaa_repo = $em->getRepository("BackendBundle:Provincia");
                                    $ccaa = $ccaa_repo->find($form->get("provinciaInscritos")->getData());
                                    $inscrito->setCcaaInscritos($ccaa->getIdCcaa());

                                    $situacion = "OK";
                                }
                            } else {
                                $provinciaotros = $form->get("provinciaotros")->getData();
                                $municipiootros = $form->get("municipiootros")->getData();
                                if ($provinciaotros == "" OR $municipiootros == "") {
                                    $situacion = "ERROR";
                                    $this->session->getFlashBag()->add('warning', 'Faltan datos de provincia/estado o municipio!');
                                } else {
                                    $situacion = "OK";
                                    $provincia = null;
                                    $municipio = null;
                                }
                            }

                            if ($situacion == "OK") {


                                $fechaInscr = new \DateTime("now");
                                $inscrito->setFechainscritpcion($fechaInscr);
                                $ip = $request->getClientIp();
                                $inscrito->setIpinscripcion($ip);


                                $bytes = random_bytes(5);
                                $random = bin2hex($bytes);

                                $inscrito->setCadenaseg($random);

                                $inscrito->setRazonBloqueo("");
                                $inscrito->setDocumento($documento);
                                $inscrito->setProvinciaInscritos($provincia);
                                $inscrito->setMunicipioInscritos($municipio);
                                $inscrito->setMunicipiootros($municipiootros);
                                $inscrito->setProvinciaotros($provinciaotros);


                                $em->persist($inscrito);
                                $flush = $em->flush();
                                if ($flush == null) {
                                    $correosEnvios = $this->getParameter('correoEnvios');
                                    $elID = $inscrito->getId();
                                    $cadenaSeg = $random . "__" . $form->get('correoUsuario')->getData() . "__" . $elID;
                                    //$url_acceso = openssl_encrypt($cadenaSeg, $situacion, $password);
                                    // $url_acceso = base64_encode($cadenaSeg);
                                    $encripta = new security_encript();
                                    $clave = $this->getParameter('secret');
                                    $url_acceso = $encripta->encrypt($cadenaSeg, $clave);
                                    $mensaje = " este es el mensaje";
                                    $mailer = $this->get('mailer');
                                    $message = $mailer->createMessage()
                                            //$message = (new \Swift_Message('Hello Email'))
                                            ->setSubject('Registro en el gestor de censos')
                                            ->setFrom($correosEnvios)
                                            ->setTo($form->get('correoUsuario')->getData())
                                            ->setContentType("text/html")
                                            ->setBody(
                                            $this->renderView(
                                                    'AppBundle:Mail:registro.html.twig', array(//plantilla de correo electronico
                                                'ip' => $ip,
                                                'inscrito' => $inscrito,
                                                'url_acceso' => $url_acceso,
                                                'mensaje' => $mensaje
                                                    )
                                            )
                                    );
                                    $result = $mailer->send($message);
                                    if ($mailer->send($message)) {

                                        $this->session->getFlashBag()->add('success', 'Se ha inscrito a:' . $form->get("nombreUsuario")->getData() . ' ' . $form->get("apellidoUsuario")->getData() . ' correctamente ');
                                        $this->session->getFlashBag()->add('success', 'REGISTRO_OK');
                                        return $this->redirectToRoute('inscritos_2');
                                    } else {

                                        $this->session->getFlashBag()->add('success', 'Se ha inscrito a:' . $form->get("nombreUsuario")->getData() . ' ' . $form->get("apellidoUsuario")->getData() . ' correctamente pero el envio de correo ha dado un error ');
                                        // $this->session->getFlashBag()->add('success', 'REGISTRO_OK');
                                        return $this->redirectToRoute('inscritos_2');
                                    }
                                } else {
                                    $this->session->getFlashBag()->add('warning', ' Hay un error y no se ha inscrito correctamente !');
                                }
                            }
                        } else {
                            $this->session->getFlashBag()->add('warning', 'Ya hay un inscrito usando esa direccion de correo o numero de documento  !');
                        }
                    }
                } else {
                    $this->session->getFlashBag()->add('warning', 'No ha marcado la casilla de verificacion que indica que no es un robot !');
                }
            } else {
                $this->session->getFlashBag()->add('warning', ' Hay un error en el formulario !');
            }
        }

        return $this->render('AppBundle:inscritos:new.html.twig', array(
                    'inscrito' => $inscrito,
                    "entries" => $entries,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Pagina de respuesta cuando  se inscribe un usuario correctamente.
     *
     */
    public function segundoAction(Request $request) {
        $indexRegistro = 4;
        $em = $this->getDoctrine()->getManager();

        $entry_repo = $em->getRepository("BackendBundle:Entries");
        $entries = $entry_repo->findBy(
                array('category' => $indexRegistro), array('id' => 'ASC')
        );

        return $this->render('AppBundle:inscritos:new2.html.twig', array(
                    "entries" => $entries,
        ));
    }

    /**
     * Validate email.
     *
     */
    public function validaCorreoAction(Request $request, $valida) {
        $digControl = substr($valida, 0, 2);
        $em = $this->getDoctrine()->getManager();
        $valida2=", error";
        $cadena = "error1__error2__error3";
        if ($digControl == "DK") {
            $encripta = new security_encript();
            $clave = $this->getParameter('secret');
            $rest = substr($valida, 2);
            $cadena = $encripta->decrypt($rest, $clave);
            $subCadena = explode("__", $cadena);
            if (empty($subCadena[0])){
              $this->session->getFlashBag()->add('warning', ' Hay un error!!, si ha copiado y pegado la url del  correo, compruebe que el código de validación esta completo y si no, contacte con el administrador del sitio');
            }else{
            $laClave = $subCadena[0];
            $correo = $subCadena[1];
            $ident = $subCadena[2];



            $inscritos = $em->getRepository("BackendBundle:Inscritos");
            $inscrito = $inscritos->findOneBy(
                    array('id' => $ident,
                'correoUsuario' => $correo,
                'cadenaseg' => $laClave), array('id' => 'ASC')
            );

//if ($inscrito->rowCount()>0){

            if (!empty ($inscrito))  {
                //es correcto

                $inscrito->setValidadocorreo("SI");

                $em->persist($inscrito);
                $flush = $em->flush();
                if ($flush == null) {
                    $valida2 = "DKp2" . $rest;

                    $this->session->getFlashBag()->add('success', ' Se ha validado su correo   ' . $correo . '   correctamente!!');
                    $this->session->getFlashBag()->add('success', 'REGISTRO_OK');
                } else {
                    $this->session->getFlashBag()->add('warning', ' Hay un error con el acceso a la base de datos!! contacte con el administrador del sitio');
                }
            } else {

                //hay un error
                $this->session->getFlashBag()->add('warning', ' Hay un error!!, si ha copiado y pegado la url del  correo, compruebe que el código de validación esta completo y si no, contacte con el administrador del sitio');
            }
          }
        } else {
            $this->session->getFlashBag()->add('warning', ' Hay un error!!, si ha copiado y pegado la url del  correo, compruebe que el código de validación esta completo y si no, contacte con el administrador del sitio');
        }

        return $this->render('AppBundle:inscritos:validaCorreo.html.twig', array(
                    //"incrito" => $inscrito,
                    "valida2" => $valida2,
        ));
    }

    /**
     * Recupera dattos para seguir con la inscripcción.
     *
     */
    public function recuperaAction(Request $request, \Swift_Mailer $mailer) {
      //  $inscrito = new Inscritos();
      //  $form = $this->createForm('AppBundle\Form\RecuperaType', $inscrito);  // creamos el formulario
        $form = $this->createForm('AppBundle\Form\RecuperaType');  // creamos el formulario
          // hacemos la busqueda de los datos/textos que van en la pagina
        $indexRegistro = 2;
        $em = $this->getDoctrine()->getManager();

        $entry_repo = $em->getRepository("BackendBundle:Entries");
        $entries = $entry_repo->findBy(
                array('category' => $indexRegistro), array('id' => 'ASC')
        );
        // Fin de los datos/texto

        $form->handleRequest($request);

        $data = $form->getData();


        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if ($this->captchaverify($request->get('g-recaptcha-response'))) {

//$propertyAccessor = PropertyAccess::createPropertyAccessor();
              //  $email=$form->get("correoUsuario")->getData();
                $documento = str_replace(' ', '', $form->get("documento")->getData()); // quitamos los espacios en blanco
                $documento = str_replace('-', '', $documento); // quitamos si han puesto un guion entre los numeros y la letra

              //  $inscritos = $em->getRepository("BackendBundle:Inscritos");
              //  $inscrito_isset = $inscritos->findOneBy(
                //        array('email' => $email,
              //                'doc' => $documento));

                $query = $em->createQuery('SELECT u FROM BackendBundle:Inscritos u WHERE u.correoUsuario = :email OR u.documento = :doc')
                        ->setParameter('email', $form->get("correoUsuario")->getData())
                        ->setParameter('doc', $documento);

              //  $inscrito_isset = $query->getResult();
                $inscrito_isset = $query->setMaxResults(1)->getOneOrNullResult();
                if (!empty ($inscrito_isset))  {
                //if (count($inscrito_isset) == 1) {

                    $ip = $request->getClientIp();

                    $correosEnvios = $this->getParameter('correoEnvios');

                    $random = $inscrito_isset->getCadenaseg();
                    $elID = $inscrito_isset->getId();

                    $cadenaSeg = $random . "__" . $form->get('correoUsuario')->getData() . "__" . $elID;
                    //$url_acceso = openssl_encrypt($cadenaSeg, $situacion, $password);
                    // $url_acceso = base64_encode($cadenaSeg);

                    $encripta = new security_encript();
                    $clave = $this->getParameter('secret');
                    $url_acceso = $encripta->encrypt($cadenaSeg, $clave);
                    $mensaje = " este es el mensaje";
                    $mailer = $this->get('mailer');
                    $message = $mailer->createMessage()
                            //$message = (new \Swift_Message('Hello Email'))
                            ->setSubject('Registro en el gestor de censos')
                            ->setFrom($correosEnvios)
                            ->setTo($form->get('correoUsuario')->getData())
                            ->setContentType("text/html")
                            ->setBody(
                            $this->renderView(
                                'AppBundle:Mail:recupera.html.twig', array(//plantilla de correo electronico
                                'ip' => $ip,
                                'inscrito' => $inscrito_isset,
                                'url_acceso' => $url_acceso,
                                'mensaje' => $mensaje
                                    )
                            )
                    );
                    $result = $mailer->send($message);
                    if ($mailer->send($message)) {

                        $this->session->getFlashBag()->add('success', 'Se ha enviado un correo electronico para que pueda finalizar el proceso');
                        $this->session->getFlashBag()->add('success', 'REGISTRO_OK');
                        return $this->redirectToRoute('inscritos_2');
                    } else {

                        $this->session->getFlashBag()->add('success', 'El envio de correo ha dado un error!!! ');
                        // $this->session->getFlashBag()->add('success', 'REGISTRO_OK');
                        return $this->redirectToRoute('irecupera_2');
                    }
                } else {
                    $this->session->getFlashBag()->add('warning', 'Hay un error!!! . No hay un inscrito usando esa direccion de correo o numero de documento!');
                }

               } else {
                $this->session->getFlashBag()->add('warning', 'No ha marcado la casilla de verificacion que indica que no es un robot !');
               }
            } else {
                $this->session->getFlashBag()->add('warning', ' Hay un error en el formulario !');
           }
        }

        return $this->render('AppBundle:inscritos:recupera.html.twig', array(
                  //  'inscrito' => $inscrito,
                    "entries" => $entries,
                    'form' => $form->createView(),
        ));
    }

        public function recupera2Action(Request $request) {
        $indexRegistro = 6;
        $em = $this->getDoctrine()->getManager();

        $entry_repo = $em->getRepository("BackendBundle:Entries");
        $entries = $entry_repo->findBy(
                array('category' => $indexRegistro), array('id' => 'ASC')
        );

        return $this->render('AppBundle:inscritos:recupera2.html.twig', array(
                    "entries" => $entries,
        ));
    }

    # get success response from recaptcha and return it to controller

    function captchaverify($recaptcha) {
        $reCAPTCHA_secret = $this->getParameter('reCAPTCHA_secret');
        $url = "https://www.google.com/recaptcha/api/siteverify";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
            "secret" => $reCAPTCHA_secret, "response" => $recaptcha));
        $response = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($response);

        return $data->success;
    }

}
