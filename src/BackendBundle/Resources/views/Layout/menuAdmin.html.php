<div id="wrapper">
    <div class="overlay"></div>

    <!-- Sidebar -->
    <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
        <ul class="nav sidebar-nav">
            <li class="sidebar-brand">
                <a href="#">
                    ADMIN MENÚ
                </a>
            </li>
            <!--<li role="separator" class="divider"></li>-->
            <li>
                <a href="{{path('inscritos_index')}}">Inscritos</a>
            </li>
            <li>
                <a href="{{path('inscritos_pendientes')}}">Validar Inscritos</a>
            </li>
            <li>
                <a href="{{path('inscritos_validados')}}">Inscritos Validados</a>
            </li>
            <!--<li>
                <a href="{{path('inscritos_validados')}}">Buscador avanzado</a>
            </li> -->
            <li>
                <a href="{{path('others_show')}}">Mi perfil</a>
            </li>
            <li>
                <a href="{{path('others_help')}}">Ayuda</a>
            </li>
            <li>
                <div class="clearfix"></div>
                <form action="{{path('inscritos_search')}}" >

                    <input type="text" class="form-control" placeholder="Buscar Inscritos" name="search" />

                    <button type="submit" class="btn btn-glyphicon pull-right"> 
                        <span class="glyphicon glyphicon-search" aria-hidden="true"> Buscar</span> 
                    </button>

                </form>
            </li>


            <li role="separator" class="divider"></li>
            <li>
                <a href="{{ path('inscritos_new') }}">Añadir inscrito</a>

            </li>
            <li>
                <a href="{{path('entries_index')}}">Páginas</a>
            </li>   
            <li>
                <a href="{{path('users_index')}}">Administrar Administradores</a>
            </li>
            <li><a href="{{ path('descargar') }}">Descargar datos</a></li>
            <!--  <li>
                 <a href="{{path('categories_index')}}">Administrar categorías</a>
             </li>
            -->

            <!--    <li>
                  <a href="{{ path('acciones_index') }}">Administrar Acciones</a>
  
              </li>          
              <li>
                  <a href="{{path('users_index')}}">Administrar Administradores</a>
              </li> 
                <li>
                    <a href="{{ path('inscritosacciones_index') }}">añadir Acciones</a>
    
                </li>
               <li>
                    <a href="{{ path('inscritosimages_index') }}">Añadir inscrito imagen</a>
    
                </li>
              <li>
                  <a href="{{path('preferencias_index')}}">Preferencias</a>
              </li>-->


            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Más opciones <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li class="dropdown-header">Menú extendido</li>

                    <li> <a href="{{path('categories_index')}}">Administrar categorías</a></li>
                    <li><a href="{{ path('acciones_index') }}">Administrar Acciones</a></li>
                    
                   <!-- <li><a href="{{path('preferencias_index')}}">Preferencias</a></li> -->
                </ul>
            </li>

            <li role="separator" class="divider"></li>
            <li><a href="{{path('logout')}}">Salir</a></li> 

        </ul>
    </nav>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wraper">
        <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
            <span class="hamb-top"></span>
            <span class="hamb-middle"></span>
            <span class="hamb-bottom"></span>
        </button>

    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->
