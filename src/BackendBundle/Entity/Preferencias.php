<?php

namespace BackendBundle\Entity;

/**
 * Preferencias
 */
class Preferencias
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $preferencia;

    /**
     * @var string
     */
    private $dato;

    /**
     * @var \BackendBundle\Entity\Users
     */
    private $userPreferencias;


    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Preferencias
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set preferencia
     *
     * @param string $preferencia
     *
     * @return Preferencias
     */
    public function setPreferencia($preferencia)
    {
        $this->preferencia = $preferencia;

        return $this;
    }

    /**
     * Get preferencia
     *
     * @return string
     */
    public function getPreferencia()
    {
        return $this->preferencia;
    }

    /**
     * Set dato
     *
     * @param string $dato
     *
     * @return Preferencias
     */
    public function setDato($dato)
    {
        $this->dato = $dato;

        return $this;
    }

    /**
     * Get dato
     *
     * @return string
     */
    public function getDato()
    {
        return $this->dato;
    }

    /**
     * Set userPreferencias
     *
     * @param \BackendBundle\Entity\Users $userPreferencias
     *
     * @return Preferencias
     */
    public function setUserPreferencias(\BackendBundle\Entity\Users $userPreferencias = null)
    {
        $this->userPreferencias = $userPreferencias;

        return $this;
    }

    /**
     * Get userPreferencias
     *
     * @return \BackendBundle\Entity\Users
     */
    public function getUserPreferencias()
    {
        return $this->userPreferencias;
    }
    /**
     * @var \BackendBundle\Entity\Users
     */
    private $users;


    /**
     * Set users
     *
     * @param \BackendBundle\Entity\Users $users
     *
     * @return Preferencias
     */
    public function setUsers(\BackendBundle\Entity\Users $users = null)
    {
        $this->users = $users;

        return $this;
    }

    /**
     * Get users
     *
     * @return \BackendBundle\Entity\Users
     */
    public function getUsers()
    {
        return $this->users;
    }
}
