<?php

namespace BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsersType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id')->add('role')->add('email')->add('name')->add('surname')->add('password')->add('nick')->add('bio')->add('isActive')->add('image')->add('nif')->add('nivelUsuario')->add('nivelAcceso')->add('fechaUltima')->add('codigoRec')->add('bloqueo')->add('razonBloqueo')->add('fechaControl')->add('idCcaa')->add('idMunicipio')->add('idProvincia');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackendBundle\Entity\Users'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backendbundle_users';
    }


}
