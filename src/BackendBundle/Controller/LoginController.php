<?php

namespace BackendBundle\Controller;

use BackendBundle\Entity\Users;
use BackendBundle\Entity\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use BackendBundle\Repository\UserRepository;
/**
 * User controller.
 *
 */
class LoginController extends Controller {

    private $session;

    public function __construct() {
        $this->session = new Session();
    }

    public function loginAction(Request $request) {  //autentificamos usuarios usando los metodos de symfony de autentificación
        $authenticationUtils = $this->get("security.authentication_utils");
        
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        
//        if ($this->getUser() === "info@demokratian.org") {
//            $error="esta bloqueado";
//            return $this->render('AppBundle:User:login.html.twig', [
//                'last_username' => $lastUsername,
//                'error'         => $error,
//            ]);
//        }


        return $this->render('AppBundle:User:login.html.twig', array(
                    "error" => $error,
                    "last_username" => $lastUsername
        ));
    }


}
