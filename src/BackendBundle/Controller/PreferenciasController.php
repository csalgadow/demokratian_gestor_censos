<?php

namespace BackendBundle\Controller;

use BackendBundle\Entity\Preferencias;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Preferencia controller.
 *
 */
class PreferenciasController extends Controller
{
        private $session;

    public function __construct() {
        $this->session = new Session();
    }
    
    /**
     * Lists all preferencia entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $preferencias = $em->getRepository('BackendBundle:Preferencias')->findAll();

        return $this->render('BackendBundle:preferencias:index.html.twig', array(
            'preferencias' => $preferencias,
        ));
    }

    /**
     * Creates a new preferencia entity.
     *
     */
    public function newAction(Request $request)
    {
        $preferencia = new Preferencias();
        $form = $this->createForm('BackendBundle\Form\PreferenciasType', $preferencia);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($preferencia);
            $em->flush();

            return $this->redirectToRoute('preferencias_show', array('id' => $preferencia->getId()));
        }

        return $this->render('BackendBundle:preferencias:new.html.twig', array(
            'preferencia' => $preferencia,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a preferencia entity.
     *
     */
    public function showAction(Preferencias $preferencia)
    {
        $deleteForm = $this->createDeleteForm($preferencia);

        return $this->render('preferencias/show.html.twig', array(
            'preferencia' => $preferencia,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing preferencia entity.
     *
     */
    public function editAction(Request $request, Preferencias $preferencia)
    {
        $deleteForm = $this->createDeleteForm($preferencia);
        $editForm = $this->createForm('BackendBundle\Form\PreferenciasType', $preferencia);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('preferencias_edit', array('id' => $preferencia->getId()));
        }

        return $this->render('BackendBundle:preferencias:edit.html.twig', array(
            'preferencia' => $preferencia,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a preferencia entity.
     *
     */
    public function deleteAction(Request $request, Preferencias $preferencia)
    {
        $form = $this->createDeleteForm($preferencia);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($preferencia);
            $em->flush();
        }

        return $this->redirectToRoute('preferencias_index');
    }

    /**
     * Creates a form to delete a preferencia entity.
     *
     * @param Preferencias $preferencia The preferencia entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Preferencias $preferencia)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('preferencias_delete', array('id' => $preferencia->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
