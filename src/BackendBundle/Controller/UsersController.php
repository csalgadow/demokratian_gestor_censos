<?php

namespace BackendBundle\Controller;

use BackendBundle\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use DemokratianBundle\Form\UserType;
use DemokratianBundle\Form\User2Type;
use DemokratianBundle\Form\UserPassType;

/**
 * User controller.
 *
 */
class UsersController extends Controller {

    private $session;

    public function __construct() {
        $this->session = new Session();
    }

    /*
      public function loginAction(Request $request) {  //autentificamos usuarios usanod los etodos de symfony de autentificación
      $authenticationUtils = $this->get("security.authentication_utils");
      $error = $authenticationUtils->getLastAuthenticationError();
      $lastUsername = $authenticationUtils->getLastUsername();

      return $this->render('AppBundle:User:login.html.twig', array(
      "error" => $error,
      "last_username" => $lastUsername
      ));
      }
     */

    /**
     * Lists all user entities.
     *
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT e FROM BackendBundle:Users e";
        $query = $em->createQuery($dql);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, $request->query->getInt('page', 1), 15
        );


        return $this->render('BackendBundle:users:index.html.twig', array(
                    'pagination' => $pagination,
        ));
    }

    /**
     * Creates a new user entity.
     *
     */
    public function newAction(Request $request) {
        $user = new Users();
        $form = $this->createForm('BackendBundle\Form\UsersType', $user, array(
            'accion' => 'crear_usuario',
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $pass = $form->get("password")->getData();
                $factory = $this->get("security.encoder_factory"); // llamamos al servicio para usar el mismo tipo de encriptación
                $encoder = $factory->getEncoder($user); // llamamos al objeto user
                $password = $encoder->encodePassword($form->get("password")->getData(), $user->getSalt()); //codifciamos la password que ha llegado por el formulario
                $user->setPassword($password);
                $ccaa_repo = $em->getRepository("BackendBundle:Provincia");

                //echo $form->get("idProvincia")->getData();

                $ccaa = $ccaa_repo->find($form->get("idProvincia")->getData());
                $user->setIdCcaa($ccaa->getIdCcaa());

                $em->persist($user);
                $flush = $em->flush();
                if ($flush === null) {  //si el envio de datos no da error
                    if ($form->get("enviar")->getData() == "1") {
                        $correosEnvios = $this->getParameter('correoEnvios');

                        $mailer = $this->get('mailer');
                        $message = $mailer->createMessage()
                                //$message = (new \Swift_Message('Hello Email'))
                                ->setSubject('Nuevo administrador en el gestor de censos')
                                ->setFrom($correosEnvios)
                                ->setTo($form->get('email')->getData())
                                ->setContentType("text/html")
                                ->setBody(
                                $this->renderView(
                                        'AppBundle:Mail:adminNotificacion.html.twig', array(//plantilla de correo electronico
                                    'pass' => $pass,
                                    'user' => $user,
                                        )
                                )
                        );
                        $result = $mailer->send($message);
                        if ($mailer->send($message)) {

                            $this->session->getFlashBag()->add('success', 'El administrador :' . $form->get("name")->getData() . ' ' . $form->get("surname")->getData() . ' se ha creado correctamente ');
                            return $this->redirectToRoute('users_show', array('id' => $user->getId())); //redirigimos la pagina si se incluido correctamete
                        } else {

                            $this->session->getFlashBag()->add('success', 'El administrador :' . $form->get("name")->getData() . ' ' . $form->get("surname")->getData() . ' se ha creado correctamente pero el envio de correo ha dado un error ');
                            return $this->redirectToRoute('users_show', array('id' => $user->getId())); //redirigimos la pagina si se incluido correctamete
                        }
                    } else {
                        $this->session->getFlashBag()->add('success', 'El administrador :' . $form->get("name")->getData() . ' ' . $form->get("surname")->getData() . ' se ha creado correctamente. No se ha enviado ningun correocon de notificación');
                        return $this->redirectToRoute('users_show', array('id' => $user->getId())); //redirigimos la pagina si se incluido correctamete
                    }
                } else {
                    $this->session->getFlashBag()->add('warning', 'Error al añadir el usuario');
                }
            } else {
                $this->session->getFlashBag()->add('warning', '¡ El usuario no se ha creado por un error en el formulario !');
            }
        }
        return $this->render('BackendBundle:users:new.html.twig', array(
                    'user' => $user,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a user entity.
     *
     */
    public function showAction(Users $user) {
        $deleteForm = $this->createDeleteForm($user);

        return $this->render('BackendBundle:users:show.html.twig', array(
                    'user' => $user,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     */
    public function editAction(Request $request, Users $user) {
        $deleteForm = $this->createDeleteForm($user);
        $editForm = $this->createForm('BackendBundle\Form\UsersType', $user, array(
            'accion' => 'modificar_perfil',
        ));
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted()) {
            if ($editForm->isValid()) {
                $flush = $this->getDoctrine()->getManager()->flush();

                if ($flush === null) {  //si el envio de datos no da error
                    if ($editForm->get("enviar")->getData() == "1") {
                        $correosEnvios = $this->getParameter('correoEnvios');

                        $mailer = $this->get('mailer');
                        $message = $mailer->createMessage()
                                //$message = (new \Swift_Message('Hello Email'))
                                ->setSubject('Nuevo administrador en el gestor de censos')
                                ->setFrom($correosEnvios)
                                ->setTo($editForm->get('email')->getData())
                                ->setContentType("text/html")
                                ->setBody(
                                $this->renderView(
                                        'AppBundle:Mail:adminNotificacion.html.twig', array(//plantilla de correo electronico
                                    'pass' => 'su password anterior',
                                    'user' => $user,
                                        )
                                )
                        );
                        $result = $mailer->send($message);
                        if ($mailer->send($message)) {

                            $this->session->getFlashBag()->add('success', 'El administrador :' . $editForm->get("name")->getData() . ' ' . $editForm->get("surname")->getData() . ' se ha editado correctamente ');
                            return $this->redirectToRoute('users_show', array('id' => $user->getId())); //redirigimos la pagina si se incluido correctamete
                        } else {

                            $this->session->getFlashBag()->add('success', 'El administrador :' . $editForm->get("name")->getData() . ' ' . $editForm->get("surname")->getData() . ' se ha editado correctamente pero el envio de correo ha dado un error ');
                            return $this->redirectToRoute('users_show', array('id' => $user->getId())); //redirigimos la pagina si se incluido correctamete
                        }
                    } else {
                        $this->session->getFlashBag()->add('success', 'El administrador :' . $editForm->get("name")->getData() . ' ' . $editForm->get("surname")->getData() . ' se ha ecitado correctamente. No se ha enviado ningun correocon de notificación');
                        return $this->redirectToRoute('users_show', array('id' => $user->getId())); //redirigimos la pagina si se incluido correctamete
                    }
                } else {
                    $this->session->getFlashBag()->add('warning', 'Error al editar el usuario');
                }
            } else {
                $this->session->getFlashBag()->add('warning', '¡ Hay un error en el formulario !');
            }
        }

        return $this->render('BackendBundle:users:edit.html.twig', array(
                    'accion' => 'modificar_perfil',
                    'user' => $user,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a user entity.
     *
     */
    public function deleteAction(Request $request, Users $user) {
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
        }

        return $this->redirectToRoute('users_index');
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param Users $user The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Users $user) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('users_delete', array('id' => $user->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
