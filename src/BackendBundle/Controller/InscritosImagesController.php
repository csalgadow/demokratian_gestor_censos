<?php

namespace BackendBundle\Controller;

use BackendBundle\Entity\InscritosImages;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Inscritosimage controller.
 *
 */
class InscritosImagesController extends Controller
{
        private $session;

    public function __construct() {
        $this->session = new Session();
    }
    
    /**
     * Lists all inscritosImage entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $inscritosImages = $em->getRepository('BackendBundle:InscritosImages')->findAll();

        return $this->render('BackendBundle:inscritosimages:index.html.twig', array(
            'inscritosImages' => $inscritosImages,
        ));
    }

    /**
     * Creates a new inscritosImage entity.
     *
     */
    public function newAction(Request $request)
    {
        $inscritosImages = new Inscritosimages();
        $form = $this->createForm('BackendBundle\Form\InscritosImagesType', $inscritosImages);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($inscritosImages);
            $em->flush();

            return $this->redirectToRoute('inscritosimages_show', array('id' => $inscritosImages->getId()));
        }

        return $this->render('BackendBundle:inscritosimages:new.html.twig', array(
            'inscritosImages' => $inscritosImages,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a inscritosImage entity.
     *
     */
    public function showAction(InscritosImages $inscritosImage)
    {
        $deleteForm = $this->createDeleteForm($inscritosImage);

        return $this->render('BackendBundle:inscritosimages:show.html.twig', array(
            'inscritosImage' => $inscritosImage,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing inscritosImage entity.
     *
     */
    public function editAction(Request $request, InscritosImages $inscritosImage)
    {
        $deleteForm = $this->createDeleteForm($inscritosImage);
        $editForm = $this->createForm('BackendBundle\Form\InscritosImagesType', $inscritosImage);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('inscritosimages_edit', array('id' => $inscritosImage->getId()));
        }

        return $this->render('BackendBundle:inscritosimages:edit.html.twig', array(
            'inscritosImage' => $inscritosImage,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a inscritosImage entity.
     *
     */
    public function deleteAction(Request $request, InscritosImages $inscritosImage)
    {
        $form = $this->createDeleteForm($inscritosImage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($inscritosImage);
            $em->flush();
        }

        return $this->redirectToRoute('inscritosimages_index');
    }

    /**
     * Creates a form to delete a inscritosImage entity.
     *
     * @param InscritosImages $inscritosImage The inscritosImage entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(InscritosImages $inscritosImage)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('inscritosimages_delete', array('id' => $inscritosImage->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
