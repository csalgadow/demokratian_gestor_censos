<?php

namespace BackendBundle\Entity;

/**
 * Provincia
 */
class Provincia
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $provincia;

    /**
     * @var integer
     */
    private $especial;

    /**
     * @var string
     */
    private $correoNotificaciones;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userProvincia;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $municipio;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $inscritosProvincia;

    /**
     * @var \BackendBundle\Entity\Ccaa
     */
    private $idCcaa;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userProvincia = new \Doctrine\Common\Collections\ArrayCollection();
        $this->municipio = new \Doctrine\Common\Collections\ArrayCollection();
        $this->inscritosProvincia = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idMunicipio= new \Doctrine\Common\Collections\ArrayCollection;
        $this->idProvincia= new \Doctrine\Common\Collections\ArrayCollection;
        $this->idCcaa= new \Doctrine\Common\Collections\ArrayCollection;
    }

         public function __toString() {
       // return strval($this->idCcaa);
        return $this->provincia;
    }
    
    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Provincia
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set provincia
     *
     * @param string $provincia
     *
     * @return Provincia
     */
    public function setProvincia($provincia)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia
     *
     * @return string
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set especial
     *
     * @param integer $especial
     *
     * @return Provincia
     */
    public function setEspecial($especial)
    {
        $this->especial = $especial;

        return $this;
    }

    /**
     * Get especial
     *
     * @return integer
     */
    public function getEspecial()
    {
        return $this->especial;
    }

    /**
     * Set correoNotificaciones
     *
     * @param string $correoNotificaciones
     *
     * @return Provincia
     */
    public function setCorreoNotificaciones($correoNotificaciones)
    {
        $this->correoNotificaciones = $correoNotificaciones;

        return $this;
    }

    /**
     * Get correoNotificaciones
     *
     * @return string
     */
    public function getCorreoNotificaciones()
    {
        return $this->correoNotificaciones;
    }

    /**
     * Add userProvincium
     *
     * @param \BackendBundle\Entity\Users $userProvincium
     *
     * @return Provincia
     */
    public function addUserProvincium(\BackendBundle\Entity\Users $userProvincium)
    {
        $this->userProvincia[] = $userProvincium;

        return $this;
    }

    /**
     * Remove userProvincium
     *
     * @param \BackendBundle\Entity\Users $userProvincium
     */
    public function removeUserProvincium(\BackendBundle\Entity\Users $userProvincium)
    {
        $this->userProvincia->removeElement($userProvincium);
    }

    /**
     * Get userProvincia
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserProvincia()
    {
        return $this->userProvincia;
    }

    /**
     * Add municipio
     *
     * @param \BackendBundle\Entity\Municipios $municipio
     *
     * @return Provincia
     */
    public function addMunicipio(\BackendBundle\Entity\Municipios $municipio)
    {
        $this->municipio[] = $municipio;

        return $this;
    }

    /**
     * Remove municipio
     *
     * @param \BackendBundle\Entity\Municipios $municipio
     */
    public function removeMunicipio(\BackendBundle\Entity\Municipios $municipio)
    {
        $this->municipio->removeElement($municipio);
    }

    /**
     * Get municipio
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * Add inscritosProvincium
     *
     * @param \BackendBundle\Entity\Inscritos $inscritosProvincium
     *
     * @return Provincia
     */
    public function addInscritosProvincium(\BackendBundle\Entity\Inscritos $inscritosProvincium)
    {
        $this->inscritosProvincia[] = $inscritosProvincium;

        return $this;
    }

    /**
     * Remove inscritosProvincium
     *
     * @param \BackendBundle\Entity\Inscritos $inscritosProvincium
     */
    public function removeInscritosProvincium(\BackendBundle\Entity\Inscritos $inscritosProvincium)
    {
        $this->inscritosProvincia->removeElement($inscritosProvincium);
    }

    /**
     * Get inscritosProvincia
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInscritosProvincia()
    {
        return $this->inscritosProvincia;
    }

    /**
     * Set idCcaa
     *
     * @param \BackendBundle\Entity\Ccaa $idCcaa
     *
     * @return Provincia
     */
    public function setIdCcaa(\BackendBundle\Entity\Ccaa $idCcaa = null)
    {
        $this->idCcaa = $idCcaa;

        return $this;
    }

    /**
     * Get idCcaa
     *
     * @return \BackendBundle\Entity\Ccaa
     */
    public function getIdCcaa()
    {
        return $this->idCcaa;
    }
}
