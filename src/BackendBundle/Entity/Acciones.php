<?php

namespace BackendBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Acciones
 */
class Acciones {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $inscritosAcciones;

    /**
     * Constructor
     */
    public function __construct() {
        $this->id = new ArrayCollection();
        $this->inscritosAcciones = new ArrayCollection();
    }

    public function __toString() {
    return $this->name;
}

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Acciones
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Acciones
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Add inscritosAccione
     *
     * @param \BackendBundle\Entity\InscritosAcciones $inscritosAccione
     *
     * @return Acciones
     */
    public function addInscritosAccione(\BackendBundle\Entity\InscritosAcciones $inscritosAccione) {
        $this->inscritosAcciones[] = $inscritosAccione;

        return $this;
    }

    /**
     * Remove inscritosAccione
     *
     * @param \BackendBundle\Entity\InscritosAcciones $inscritosAccione
     */
    public function removeInscritosAccione(\BackendBundle\Entity\InscritosAcciones $inscritosAccione) {
        $this->inscritosAcciones->removeElement($inscritosAccione);
    }

    /**
     * Get inscritosAcciones
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInscritosAcciones() {
        return $this->inscritosAcciones;
    }

}
