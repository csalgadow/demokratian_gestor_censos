<?php

namespace BackendBundle\Controller;

use BackendBundle\Entity\Acciones;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Accione controller.
 *
 */
class AccionesController extends Controller
{
    /**
     * Lists all accione entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $acciones = $em->getRepository('BackendBundle:Acciones')->findAll();

        return $this->render('BackendBundle:acciones:index.html.twig', array(
            'acciones' => $acciones,
        ));
    }

    /**
     * Creates a new accione entity.
     *
     */
    public function newAction(Request $request)
    {
        $acciones = new Acciones();
        $form = $this->createForm('BackendBundle\Form\AccionesType', $acciones);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($acciones);
            $em->flush();

            return $this->redirectToRoute('acciones_show', array('id' => $acciones->getId()));
        }

        return $this->render('BackendBundle:acciones:new.html.twig', array(
            'accione' => $acciones,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a accione entity.
     *
     */
    public function showAction(Acciones $accione)
    {
        $deleteForm = $this->createDeleteForm($accione);

        return $this->render('BackendBundle:acciones:show.html.twig', array(
            'accione' => $accione,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing accione entity.
     *
     */
    public function editAction(Request $request, Acciones $accione)
    {
        $deleteForm = $this->createDeleteForm($accione);
        $editForm = $this->createForm('BackendBundle\Form\AccionesType', $accione);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('acciones_edit', array('id' => $accione->getId()));
        }

        return $this->render('BackendBundle:acciones:edit.html.twig', array(
            'accione' => $accione,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a accione entity.
     *
     */
    public function deleteAction(Request $request, Acciones $accione)
    {
        $form = $this->createDeleteForm($accione);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($accione);
            $em->flush();
        }

        return $this->redirectToRoute('acciones_index');
    }

    /**
     * Creates a form to delete a accione entity.
     *
     * @param Acciones $accione The accione entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Acciones $accione)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('acciones_delete', array('id' => $accione->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
