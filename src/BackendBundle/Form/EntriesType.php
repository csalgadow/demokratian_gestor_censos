<?php

namespace BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;


class EntriesType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('title', TextType::class, array("label" => "Titulo: ",
                    "required" => "required",
                    "attr" => array("class" => "form-name form-control")))
                ->add('content', TextareaType::class, array("label" => "Contenido: ", "required" => false,
                    "attr" => array("class" => "form-name form-control")))
                ->add('status', ChoiceType::class, array("label" => "Estado: ",
                    "choices" => array(
                        "Publico" => "public",
                        "Privado" => "private"),
                    "attr" => array("class" => "form-name form-control")))
                ->add('category', EntityType::class, array(
                    "label" => "Categorias: ",
                    "class" => "BackendBundle:Categories",
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                                
                                ->where('u.id > 6')
                                ->orderBy('u.name', 'ASC');
                    },
                    "attr" => array("class" => "form-name form-control")))
                ->add('image', FileType::class, array("label" => "Imagen: ",
                    "attr" => array("class" => "form-name form-control"),
                    "data_class" => null,
                    "required" => false
                ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'BackendBundle\Entity\Entries'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'backendbundle_entries';
    }

}
