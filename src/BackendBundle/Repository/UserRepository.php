<?php

namespace BackendBundle\Repository;

use BackendBundle\Entity\Users;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Doctrine\ORM\EntityRepository;


class UserRepository extends EntityRepository implements UserLoaderInterface {

    public function loadUserByUsername($username) {
        return $this->createQueryBuilder('u')
        //$user = $this->createQueryBuilder('u')
                ->where('u.nick = :nick OR u.email = :email')
                ->setParameter('nick', $username)
                ->setParameter('email', $username)
                ->getQuery()
                ->getOneOrNullResult();

//        if (null === $user) {
//            $message = sprintf(
//                    'Unable to find an active admin AppBundle:User object identified by "%s".', $username
//            );
//            throw new UsernameNotFoundException($message);
//        }
//
//        return $user;
    }

}
