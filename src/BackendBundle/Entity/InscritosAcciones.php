<?php

namespace BackendBundle\Entity;

/**
 * InscritosAcciones
 */
class InscritosAcciones
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var string
     */
    private $accion;

    /**
     * @var string
     */
    private $datos;

    /**
     * @var \DateTime
     */
    private $fecha = 'CURRENT_TIMESTAMP';

    /**
     * @var integer
     */
    private $inscritosId;

    /**
     * @var \BackendBundle\Entity\Inscritos
     */
    private $inscritos;

    /**
     * @var \BackendBundle\Entity\Acciones
     */
    private $acciones;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return InscritosAcciones
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set accion
     *
     * @param string $accion
     *
     * @return InscritosAcciones
     */
    public function setAccion($accion)
    {
        $this->accion = $accion;

        return $this;
    }

    /**
     * Get accion
     *
     * @return string
     */
    public function getAccion()
    {
        return $this->accion;
    }

    /**
     * Set datos
     *
     * @param string $datos
     *
     * @return InscritosAcciones
     */
    public function setDatos($datos)
    {
        $this->datos = $datos;

        return $this;
    }

    /**
     * Get datos
     *
     * @return string
     */
    public function getDatos()
    {
        return $this->datos;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return InscritosAcciones
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set inscritosId
     *
     * @param integer $inscritosId
     *
     * @return InscritosAcciones
     */
    public function setInscritosId($inscritosId)
    {
        $this->inscritosId = $inscritosId;

        return $this;
    }

    /**
     * Get inscritosId
     *
     * @return integer
     */
    public function getInscritosId()
    {
        return $this->inscritosId;
    }

    /**
     * Set inscritos
     *
     * @param \BackendBundle\Entity\Inscritos $inscritos
     *
     * @return InscritosAcciones
     */
    public function setInscritos(\BackendBundle\Entity\Inscritos $inscritos = null)
    {
        $this->inscritos = $inscritos;

        return $this;
    }

    /**
     * Get inscritos
     *
     * @return \BackendBundle\Entity\Inscritos
     */
    public function getInscritos()
    {
        return $this->inscritos;
    }

    /**
     * Set acciones
     *
     * @param \BackendBundle\Entity\Acciones $acciones
     *
     * @return InscritosAcciones
     */
    public function setAcciones(\BackendBundle\Entity\Acciones $acciones = null)
    {
        $this->acciones = $acciones;

        return $this;
    }

    /**
     * Get acciones
     *
     * @return \BackendBundle\Entity\Acciones
     */
    public function getAcciones()
    {
        return $this->acciones;
    }
}

