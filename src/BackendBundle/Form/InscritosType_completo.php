<?php

namespace BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InscritosType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('nombreUsuario')
                ->add('apellidoUsuario')
                ->add('telefono')
                ->add('correoUsuario')
                ->add('validadocorreo')
                ->add('tipodocumento')
                ->add('documento')
                ->add('fechanacimiento')
                ->add('pais')
                ->add('provinciaotros')
                ->add('municipiootros')
                ->add('codigopostal')
                ->add('direccion')
                ->add('lugarparticipacion')
                ->add('fechainscritpcion')
                ->add('ipinscripcion')
                ->add('cadenaseg')
                ->add('bloqueo')
                ->add('razonBloqueo')
                ->add('perfil')
                ->add('fechaControl')
                ->add('idMunicipio')
                ->add('idProvincia')
                ->add('idCcaa')
                ->add('ccaaInscritos')
                ->add('municipioInscritos')
                ->add('provinciaInscritos');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BackendBundle\Entity\Inscritos'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'backendbundle_inscritos';
    }


}
