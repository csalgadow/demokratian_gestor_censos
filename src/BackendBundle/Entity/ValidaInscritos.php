<?php

namespace BackendBundle\Entity;

/**
 * ValidaInscritos
 */
class ValidaInscritos
{
    /**
     * @var integer
     */
    private $id;

     /**
     * @var \BackendBundle\Entity\Users
     */
    private $user;

    /**
     * @var \BackendBundle\Entity\Inscritos
     */
    private $inscrito;


    /**
     * Set id
     *
     * @param integer $id
     *
     * @return ValidaInscritos
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
     /**
     * Set user
     *
     * @param \BackendBundle\Entity\Users $user
     *
     * @return ValidaInscritos
     */
    public function setUser(\BackendBundle\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \BackendBundle\Entity\Users
     */
    public function getUser()
    {
        return $this->user;
    }

   /**
     * Set inscrito
     *
     * @param \BackendBundle\Entity\Inscritos $inscrito
     *
     * @return ValidaInscritos
     */
    public function setEntry(\BackendBundle\Entity\Inscritos $inscrito = null)
    {
        $this->inscrito = $inscrito;

        return $this;
    }


    /**
     * Get inscrito
     *
     * @return \BackendBundle\Entity\Inscritos
     */
    public function getInscrito()
    {
        return $this->inscrito;
    }
    
    /**
     * @var \BackendBundle\Entity\Users
     */
    private $users;

    /**
     * @var \BackendBundle\Entity\Inscritos
     */
    private $inscritos;


    /**
     * Set users
     *
     * @param \BackendBundle\Entity\Users $users
     *
     * @return ValidaInscritos
     */
    public function setUsers(\BackendBundle\Entity\Users $users = null)
    {
        $this->users = $users;

        return $this;
    }

    /**
     * Get users
     *
     * @return \BackendBundle\Entity\Users
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set inscritos
     *
     * @param \BackendBundle\Entity\Inscritos $inscritos
     *
     * @return ValidaInscritos
     */
    public function setInscritos(\BackendBundle\Entity\Inscritos $inscritos = null)
    {
        $this->inscritos = $inscritos;

        return $this;
    }

    /**
     * Get inscritos
     *
     * @return \BackendBundle\Entity\Inscritos
     */
    public function getInscritos()
    {
        return $this->inscritos;
    }
}
