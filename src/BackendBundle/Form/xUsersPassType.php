<?php

namespace BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
//use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;


class UsersPassType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('oldPassword', PasswordType::class, array(
                    'label' => 'Ponga su password actual',
                   // 'mapped' => false,
                   // 'constraints' => new UserPassword()
                    ))
                ->add('password', RepeatedType::class, array(
                    "required" => "required",
                    'type' => PasswordType::class,
                    'invalid_message' => 'Los dos password deben coincidir',
                    'first_options' => array('label' => 'Password nuevo', "attr" => array("class" => "form-password form-control")),
                    'second_options' => array('label' => 'Repita el Password nuevo', "attr" => array("class" => "form-password form-control"))
                        )
                )
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'BackendBundle\Entity\Users',
                //'accion' => 'modificar_perfil',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'backendbundle_users';
    }

}
