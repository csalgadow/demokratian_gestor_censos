<?php

namespace BackendBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
/**
 * Categories
 */
class Categories {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $entry;

    /**
     * Constructor
     */
    public function __construct() {
        $this->entry = new ArrayCollection();
    }

    public function __toString() {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Categories
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Categories
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Add entry
     *
     * @param \BackendBundle\Entity\Entries $entry
     *
     * @return Categories
     */
    public function addEntry(\BackendBundle\Entity\Entries $entry) {
        $this->entry[] = $entry;

        return $this;
    }

    /**
     * Remove entry
     *
     * @param \BackendBundle\Entity\Entries $entry
     */
    public function removeEntry(\BackendBundle\Entity\Entries $entry) {
        $this->entry->removeElement($entry);
    }

    /**
     * Get entry
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEntry() {
        return $this->entry;
    }

}
