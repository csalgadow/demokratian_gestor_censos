<?php

namespace BackendBundle\Controller;

use BackendBundle\Entity\ValidaInscritos;
use BackendBundle\Entity\Inscritos;
use BackendBundle\Entity\InscritosAcciones;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Validainscrito controller.
 *
 */
class ValidaInscritosController extends Controller {

    private $session;

    public function __construct() {
        $this->session = new Session();
    }

    /**
     * Lists all validaInscrito entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $validaInscritos = $em->getRepository('BackendBundle:ValidaInscritos')->findAll();

        return $this->render('BackendBundle:validainscritos:index.html.twig', array(
                    'validaInscritos' => $validaInscritos,
        ));
    }

    /**
     * Creates a new validaInscrito entity.
     *
     */
    public function newAction($idInscrito) {
        $validaInscrito = new Validainscritos();
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $validaInscrito->setUsers($user);

        $inscrito = new \BackendBundle\Entity\Inscritos();
        $inscritos = $em->getRepository("BackendBundle:Inscritos");
        $elInscrito = $inscritos->findOneById($idInscrito);


        $validaInscrito->setInscritos($elInscrito);


        $em->persist($validaInscrito);
        $flush = $em->flush();
        if ($flush == null) {
            $this->session->getFlashBag()->add('success', 'Ha validado correctamente a:' . $elInscrito->getNombreUsuario() . '  ' . $elInscrito->getApellidoUsuario() . ' .');
            $valida_repo = $em->getRepository("BackendBundle:ValidaInscritos");
            $validados = $valida_repo->findBy(array('inscritos' => $idInscrito));  //inscritos_ID

            $numeroValidados = $this->getParameter('numeroValidadores');

            if (count($validados) >= $numeroValidados) {
                $inscritosAcciones = new \BackendBundle\Entity\InscritosAcciones();
                // añadimos acciones

                $tipo = 2;
                $acciones = $em->getRepository("BackendBundle:Acciones");
                $accion = $acciones->findOneBy(
                        array('id' => $tipo)
                );


                $inscritosAcciones->setAcciones($accion);

                $inscritosAcciones->setInscritos($elInscrito);
                $fecha = new \DateTime("now");
                $inscritosAcciones->setFecha($fecha);
                $inscritosAcciones->setAccion("incluido automaticamente por el sistema");
                $em->persist($inscritosAcciones);
                $flush2 = $em->flush();
                if ($flush2 == null) {
                    $this->session->getFlashBag()->add('success', 'Ha incluido la accion de Validado.');
                } else {
                    $this->session->getFlashBag()->add('warning', ' Hay un error y no se ha incluido la accion de validado (avise al superadministrador) !');
                }
            }
        } else {
            $this->session->getFlashBag()->add('warning', ' Hay un error y no se ha validado correctamente !');
        }

        return $this->redirectToRoute('inscritos_index');
    }

    /**
     * Finds and displays a validaInscrito entity.
     *
     */
    public function showAction(ValidaInscritos $validaInscrito, $idInscrito) {
        $deleteForm = $this->createDeleteForm($validaInscrito);

        return $this->render('BackendBundle:validainscritos:show.html.twig', array(
                    'validaInscrito' => $validaInscrito,
                    'delete_form' => $deleteForm->createView(),
                    'idInscrito' => $idInscrito,
        ));
    }

    /**
     * Displays a form to edit an existing validaInscrito entity.
     *
     */
    public function editAction(Request $request, ValidaInscritos $validaInscrito) {
        $deleteForm = $this->createDeleteForm($validaInscrito);
        $editForm = $this->createForm('BackendBundle\Form\ValidaInscritosType', $validaInscrito);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('validainscritos_edit', array('id' => $validaInscrito->getId()));
        }

        return $this->render('BackendBundle:validainscritos:edit.html.twig', array(
                    'validaInscrito' => $validaInscrito,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a validaInscrito entity.
     *
     */
    public function deleteAction(Request $request, ValidaInscritos $validaInscrito) {
        $form = $this->createDeleteForm($validaInscrito);
        $form->handleRequest($request);

        $idInscrito = $validaInscrito->getInscritos();  //cogemos el id del inscrito para volver a la pagina de los validadores de estes inscrito

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($validaInscrito);  // Borramos el inscrito
            $flush = $em->flush();
            if ($flush == null) {
                $this->session->getFlashBag()->add('success', 'Borrado corectamente el validador .');
                $valida_repo = $em->getRepository("BackendBundle:ValidaInscritos");
                $validados = $valida_repo->findBy(array('inscritos' => $idInscrito));  //inscritos_ID

                $numeroValidados = $this->getParameter('numeroValidadores');

                if (count($validados) < $numeroValidados) {
                    $inscritosAcciones_repo = $em->getRepository("BackendBundle:InscritosAcciones");
                    $accionValidado = $inscritosAcciones_repo->findBy(array('inscritos' => $idInscrito, 'status' => 2));  //inscritos_ID
                    if (count($accionValidado) >= 1) {
                        foreach ($accionValidado as $accion) { // 
                            $em->remove($accion);
                            $flush2 = $em->flush();
                            if ($flush2 == null) {
                                $this->session->getFlashBag()->add('success', ' y tambien la accion de "validado" .');
                            }
                        }
                    }
                }
            }
        }

        return $this->redirectToRoute('validainscritos_inscrito', array('id' => $idInscrito));
    }

    /**
     * Creates a form to delete a validaInscrito entity.
     *
     * @param ValidaInscritos $validaInscrito The validaInscrito entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ValidaInscritos $validaInscrito) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('validainscritos_delete', array('id' => $validaInscrito->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    public function validadoresAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $inscrito = $em->getRepository('BackendBundle:Inscritos')->findOneById($id);

        //$product = $repository->findOneById($id);
//        $dql = "SELECT e FROM BackendBundle:ValidaInscritos e WHERE e.inscritos = $id";
//        $query = $em->createQuery($dql);
//
//        $paginator = $this->get('knp_paginator');
//        $pagination = $paginator->paginate(
//                $query, $request->query->getInt('page', 1), 15
//        );



        return $this->render('BackendBundle:validainscritos:validoresIndex.html.twig', array(
                    //           'pagination' => $pagination,
                    'inscrito' => $inscrito
        ));
    }

    public function userValidadoresAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        // $validaInscritos = $em->getRepository('BackendBundle:ValidaInscritos')->findAll();
        //  $product = $repository->findOneById($id);
        $dql = "SELECT e FROM BackendBundle:ValidaInscritos e WHERE e.users = $id";
        $query = $em->createQuery($dql);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, $request->query->getInt('page', 1), 50
        );


        return $this->render('BackendBundle:validainscritos:index.html.twig', array(
                    'pagination' => $pagination,
        ));
    }

}
