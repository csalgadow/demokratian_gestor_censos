<?php

namespace BackendBundle\Controller;

use BackendBundle\Entity\InscritosAcciones;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Inscritosaccione controller.
 *
 */
class InscritosAccionesController extends Controller {

    private $session;

    public function __construct() {
        $this->session = new Session();
    }

    /**
     * Lists all inscritosAccione entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $inscritosAcciones = $em->getRepository('BackendBundle:InscritosAcciones')->findAll();

        return $this->render('BackendBundle:inscritosacciones:index.html.twig', array(
                    'inscritosAcciones' => $inscritosAcciones,
        ));
    }

    /**
     * Creates a new inscritosAccione entity.
     *
     */
    public function newAction(Request $request, $idInscrito) {

        if (in_array('ROLE_SUPER_ADMIN', $this->getUser()->getRoles())) {
            $accion = 'SUPER_ADMIN'; //variable para decirle al formulario que es un super administradores
        } else {
            $accion = 'ADMIN';
        }


        $inscritosAcciones = new Inscritosacciones();
        $form = $this->createForm('BackendBundle\Form\InscritosAccionesType', $inscritosAcciones, array(
            'accion' => $accion, //mandamos la informacion para que nos enseñe o no alguna de las opciones del formulario
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $inscritos = $em->getRepository("BackendBundle:Inscritos");
            $inscrito = $inscritos->findOneBy(
                    array('id' => $idInscrito)
            );
            if (!empty ($inscrito)){
          //  if (count($inscrito) == 1) {
                //  $dato = $form->get('status')->getData()->getId();

                if ($form->get("status")->getData()->getId() == "3") {  // si es una accion de enviar un correo
                    $correosEnvios = $this->getParameter('correoEnvios');
                    $asunto = $form->get("accion")->getData();
                    $textoInicial = $form->get("datos")->getData();

                    $nombre = $inscrito->getNombreUsuario();
                    $apellido = $inscrito->getApellidoUsuario();
                    $telefono = $inscrito->getTelefono();
                    $correo = $inscrito->getCorreoUsuario();
                    $documento = $inscrito->getDocumento();
                    $fechains = $inscrito->getFechainscritpcion();
                    $horainscritpcion = date_format($fechains, 'H:i:s');
                    $fechainscritpcion = date_format($fechains, 'd-m-Y');
                    $fechainscritpcion = $fechainscritpcion . ' a las ' . $horainscritpcion;

                    $cadenas = array('[NOMBRE]', '[APELLIDOS]', '[TELEFONO]', '[CORREO]', '[DOCUMENTO]', '[FECHAINSCRIPCION]');
                    $reemplazo = array($nombre, $apellido, $telefono, $correo, $documento, $fechainscritpcion);
                    $texto = str_replace($cadenas, $reemplazo, $textoInicial);

                    $order = array("\r\n", "\n", "\r");
                    $replace = '<br/>';
                    $nuevotexto = str_replace($order, $replace, $texto);

                    $inscritosAcciones->setAccion($asunto);
                    $inscritosAcciones->setDatos($texto); // usamos la versión del texto que no ha cambiado por br/ los saltos de lienas
                    $mailer = $this->get('mailer');
                    $message = $mailer->createMessage()
                            //$message = (new \Swift_Message('Hello Email'))
                            ->setSubject($asunto)
                            ->setFrom($correosEnvios)
                            ->setTo($correo)
                            ->setContentType("text/html")
                            ->setBody(
                            $this->renderView(
                                    'AppBundle:Mail:accionEnvioCorreo.html.twig', array(//plantilla de correo electronico
                                'texto' => $nuevotexto,
                                'inscrito' => $inscrito,
                                    )
                            )
                    );
                    $result = $mailer->send($message);
                    if ($mailer->send($message)) {
                        $this->session->getFlashBag()->add('success', 'Se ha enviado el correo correctamente ');
                    } else {
                        $this->session->getFlashBag()->add('warning', 'El envio de correo ha dado un error ');
                    }
                }

                $fechaInscr = new \DateTime("now");
                $inscritosAcciones->setFecha($fechaInscr);
//                $inscritosAcciones->setAccion($asunto);
//                $inscritosAcciones->setDatos($texto); // usamos la versión del texto que no ha cambiado por br/ los saltos de lienas
                $inscritosAcciones->setInscritos($inscrito);
                $inscritosAcciones->setAcciones($form->get("status")->getData());
                //$inscritosAcciones->setStatus($form->get("status")->getData());
                //$em = $this->getDoctrine()->getManager();
                $em->persist($inscritosAcciones);
                $flush = $em->flush();
            }
            if ($flush === null) {
                $this->session->getFlashBag()->add('success', 'La accion  se ha creado correctamente ');
                return $this->redirectToRoute('inscritos_show', array('id' => $idInscrito)); //redirigimos la pagina si se incluido correctamete
            } else {
                $this->session->getFlashBag()->add('warning', 'Error al añadir la acción');
            }
        }



        return $this->render('BackendBundle:inscritosacciones:new.html.twig', array(
                    'inscritosAccione' => $inscritosAcciones,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a inscritosAccione entity.
     *
     */
    public function showAction(InscritosAcciones $inscritosAcciones) {
        $deleteForm = $this->createDeleteForm($inscritosAcciones);

        return $this->render('BackendBundle:inscritosacciones:show.html.twig', array(
                    'inscritosAcciones' => $inscritosAcciones,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing inscritosAccione entity.
     *
     */
    public function editAction(Request $request, InscritosAcciones $inscritosAcciones) {

//        if (in_array('ROLE_SUPER_ADMIN', $this->getUser()->getRoles())) {
//            $accion = 'SUPER_ADMIN'; //variable para decirle al formulario que es un super administrador
//        } else {
//            $accion = 'ADMIN';
//        }

        $accion = 'EDIT'; //no queremos que el usaurio pueda cambiar la accion aunque sea administrador

        $deleteForm = $this->createDeleteForm($inscritosAcciones);
        $editForm = $this->createForm('BackendBundle\Form\InscritosAccionesType', $inscritosAcciones, array(
            'accion' => $accion, //mandamos la informacion para que nos enseñe o no alguna de las opciones del formulario
        ));
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('inscritosacciones_edit', array('id' => $inscritosAcciones->getId()));
        }

        return $this->render('BackendBundle:inscritosacciones:edit.html.twig', array(
                    'inscritosAcciones' => $inscritosAcciones,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a inscritosAccione entity.
     *
     */
    public function deleteAction(Request $request, InscritosAcciones $inscritosAcciones) {
        $form = $this->createDeleteForm($inscritosAcciones);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($inscritosAcciones);
            $em->flush();
        }

        return $this->redirectToRoute('inscritosacciones_index');
    }

    /**
     * Creates a form to delete a inscritosAccione entity.
     *
     * @param InscritosAcciones $inscritosAccione The inscritosAccione entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(InscritosAcciones $inscritosAccione) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('inscritosacciones_delete', array('id' => $inscritosAccione->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

//    public function useraccionesAction(Request $request, $id) {
//        $em = $this->getDoctrine()->getManager();
//
//       // $inscritosAcciones = $em->getRepository('BackendBundle:InscritosAcciones')->findAll();
//
//        $dql = "SELECT e FROM BackendBundle:InscritosAccioness e WHERE e.users = $id";
//        $query = $em->createQuery($dql);
//
//        $paginator = $this->get('knp_paginator');
//        $pagination = $paginator->paginate(
//                $query, $request->query->getInt('page', 1), 50
//        );
//
//        return $this->render('BackendBundle:inscritosacciones:index.html.twig', array(
//                    'inscritosAcciones' => $inscritosAcciones,
//        ));
//    }

}
