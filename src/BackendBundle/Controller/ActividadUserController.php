<?php

namespace BackendBundle\Controller;

use BackendBundle\Entity\ActividadUser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Actividaduser controller.
 *
 */
class ActividadUserController extends Controller
{
    /**
     * Lists all actividadUser entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $actividadUsers = $em->getRepository('BackendBundle:ActividadUser')->findAll();

        return $this->render('actividaduser/index.html.twig', array(
            'actividadUsers' => $actividadUsers,
        ));
    }

    /**
     * Creates a new actividadUser entity.
     *
     */
    public function newAction(Request $request)
    {
        $actividadUser = new Actividaduser();
        $form = $this->createForm('BackendBundle\Form\ActividadUserType', $actividadUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($actividadUser);
            $em->flush();

            return $this->redirectToRoute('actividaduser_show', array('id' => $actividadUser->getId()));
        }

        return $this->render('actividaduser/new.html.twig', array(
            'actividadUser' => $actividadUser,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a actividadUser entity.
     *
     */
    public function showAction(ActividadUser $actividadUser)
    {
        $deleteForm = $this->createDeleteForm($actividadUser);

        return $this->render('actividaduser/show.html.twig', array(
            'actividadUser' => $actividadUser,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing actividadUser entity.
     *
     */
    public function editAction(Request $request, ActividadUser $actividadUser)
    {
        $deleteForm = $this->createDeleteForm($actividadUser);
        $editForm = $this->createForm('BackendBundle\Form\ActividadUserType', $actividadUser);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('actividaduser_edit', array('id' => $actividadUser->getId()));
        }

        return $this->render('actividaduser/edit.html.twig', array(
            'actividadUser' => $actividadUser,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a actividadUser entity.
     *
     */
    public function deleteAction(Request $request, ActividadUser $actividadUser)
    {
        $form = $this->createDeleteForm($actividadUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($actividadUser);
            $em->flush();
        }

        return $this->redirectToRoute('actividaduser_index');
    }

    /**
     * Creates a form to delete a actividadUser entity.
     *
     * @param ActividadUser $actividadUser The actividadUser entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ActividadUser $actividadUser)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('actividaduser_delete', array('id' => $actividadUser->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
