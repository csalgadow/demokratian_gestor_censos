<?php

namespace BackendBundle\Entity;

/**
 * Ccaa
 */
class Ccaa
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $ccaa;

    /**
     * @var integer
     */
    private $especial;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $userCcaa;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $inscritosCcaa;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $provincia;
     private $idCcaa;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userCcaa = new \Doctrine\Common\Collections\ArrayCollection();
        $this->inscritosCcaa = new \Doctrine\Common\Collections\ArrayCollection();
        $this->provincia = new \Doctrine\Common\Collections\ArrayCollection();
    }

    
     public function __toString() {
       // return strval($this->idCcaa);
        return $this->ccaa;
    }
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ccaa
     *
     * @param string $ccaa
     *
     * @return Ccaa
     */
    public function setCcaa($ccaa)
    {
        $this->ccaa = $ccaa;

        return $this;
    }

    /**
     * Get ccaa
     *
     * @return string
     */
    public function getCcaa()
    {
        return $this->ccaa;
    }

    /**
     * Set especial
     *
     * @param integer $especial
     *
     * @return Ccaa
     */
    public function setEspecial($especial)
    {
        $this->especial = $especial;

        return $this;
    }

    /**
     * Get especial
     *
     * @return integer
     */
    public function getEspecial()
    {
        return $this->especial;
    }

    /**
     * Add userCcaa
     *
     * @param \BackendBundle\Entity\Users $userCcaa
     *
     * @return Ccaa
     */
    public function addUserCcaa(\BackendBundle\Entity\Users $userCcaa)
    {
        $this->userCcaa[] = $userCcaa;

        return $this;
    }

    /**
     * Remove userCcaa
     *
     * @param \BackendBundle\Entity\Users $userCcaa
     */
    public function removeUserCcaa(\BackendBundle\Entity\Users $userCcaa)
    {
        $this->userCcaa->removeElement($userCcaa);
    }

    /**
     * Get userCcaa
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserCcaa()
    {
        return $this->userCcaa;
    }

    /**
     * Add inscritosCcaa
     *
     * @param \BackendBundle\Entity\Inscritos $inscritosCcaa
     *
     * @return Ccaa
     */
    public function addInscritosCcaa(\BackendBundle\Entity\Inscritos $inscritosCcaa)
    {
        $this->inscritosCcaa[] = $inscritosCcaa;

        return $this;
    }

    /**
     * Remove inscritosCcaa
     *
     * @param \BackendBundle\Entity\Inscritos $inscritosCcaa
     */
    public function removeInscritosCcaa(\BackendBundle\Entity\Inscritos $inscritosCcaa)
    {
        $this->inscritosCcaa->removeElement($inscritosCcaa);
    }

    /**
     * Get inscritosCcaa
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInscritosCcaa()
    {
        return $this->inscritosCcaa;
    }

    /**
     * Add provincium
     *
     * @param \BackendBundle\Entity\Provincia $provincium
     *
     * @return Ccaa
     */
    public function addProvincium(\BackendBundle\Entity\Provincia $provincium)
    {
        $this->provincia[] = $provincium;

        return $this;
    }

    /**
     * Remove provincium
     *
     * @param \BackendBundle\Entity\Provincia $provincium
     */
    public function removeProvincium(\BackendBundle\Entity\Provincia $provincium)
    {
        $this->provincia->removeElement($provincium);
    }

    /**
     * Get provincia
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProvincia()
    {
        return $this->provincia;
    }
}
