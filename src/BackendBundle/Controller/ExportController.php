<?php

namespace BackendBundle\Controller;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\StreamedResponse;
//use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\Connection;
use BackendBundle\Entity\Inscritos;
use BackendBundle\Entity\InscritosAcciones;

//use BackendBundle\Entity\ValidaInscritos;

class ExportController extends Controller {

    public function generateFileAction(Request $request, $format, $type) {


        $response = new StreamedResponse();
        $conn = $this->get('database_connection');

        if ($type == 1) {
            $name_arch = "todos_los_inscritos";
        } else if ($type == 2) {
            $name_arch = "inscritos_validados";
        } else if ($type == 3) {
            $name_arch = "inscritos_pendientes_validar";
        } else if ($type == 4) {
            $name_arch = "inscritos_sin accion";
        }
//        if ($type == 1) {
//        $re
//        
//            $results = $conn->query('SELECT * FROM inscritos where id_provincia = 16');
//        }
//        $response->setCallback(function () use($results) {
        $response->setCallback(function () use($conn, $type, $format) {


            // Query data from database

            if ($type == 1) {

                $results = $conn->query('SELECT
                       a.nombre_usuario,
                       a.apellido_usuario,
                       a.telefono,
                       a.correo_usuario,
                       a.validadoCorreo,
                       a.tipoDocumento,
                       a.documento,
                       a.fechaNacimiento,
                       a.pais,          
                       a.id_municipio,
                       a.id_ccaa,
                       a.codigoPostal,
                       a.direccion,
                       a.lugarParticipacion,
                       a.fechaInscritpcion,
                       a.ipInscripcion,
                       a.bloqueo,
                       a.razon_bloqueo,
                       b.provincia,
                       c.nombre as municipio,
                       a.id_provincia,
                       a.id_municipio
                            FROM inscritos a,provincia b, municipios c where (a.id_provincia = b.id) and (a.id_municipio = c.id_municipio)');
            } else if ($type == 2) {

                $results = $conn->query('SELECT
                       a.nombre_usuario,
                       a.apellido_usuario,
                       a.telefono,
                       a.correo_usuario,
                       a.validadoCorreo,
                       a.tipoDocumento,
                       a.documento,
                       a.fechaNacimiento,
                       a.pais,          
                       a.id_municipio,
                       a.id_ccaa,
                       a.codigoPostal,
                       a.direccion,
                       a.lugarParticipacion,
                       a.fechaInscritpcion,
                       a.ipInscripcion,
                       a.bloqueo,
                       a.razon_bloqueo,
                       b.provincia,
                       c.nombre as municipio,
                       a.id_provincia,
                       a.id_municipio
                            FROM inscritos a,provincia b, municipios c ,inscritos_acciones d
                        where (a.id_provincia = b.id) and (a.id_municipio = c.id_municipio) and (a.ID = d.inscritos_ID) and d.status = 2 ');
            } else if ($type == 3) {

                $results = $conn->query('SELECT 
                       a.nombre_usuario,
                       a.apellido_usuario,
                       a.telefono,
                       a.correo_usuario,
                       a.validadoCorreo,
                       a.tipoDocumento,
                       a.documento,
                       a.fechaNacimiento,
                       a.pais,          
                       a.id_municipio,
                       a.id_ccaa,
                       a.codigoPostal,
                       a.direccion,
                       a.lugarParticipacion,
                       a.fechaInscritpcion,
                       a.ipInscripcion,
                       a.bloqueo,
                       a.razon_bloqueo,
                       b.provincia,
                       c.nombre as municipio,
                       a.id_provincia,
                       a.id_municipio
                            FROM inscritos a,provincia b, municipios c 
                            where (a.id_provincia = b.id) and (a.id_municipio = c.id_municipio) 
                            and  a.ID NOT IN (
                        SELECT d.inscritos_ID FROM inscritos_acciones d WHERE d.status = 2)                          
                        ');

//                SELECT m.* FROM membre m WHERE a.ID NOT IN (
//                       SELECT d.inscritos_ID FROM inscritos_acciones d WHERE d.status != 2)
            } else if ($type == 4) {  //usuarios huerfanos que no tienen ninguna accion
                $results = $conn->query('SELECT 
                       a.nombre_usuario,
                       a.apellido_usuario,
                       a.telefono,
                       a.correo_usuario,
                       a.validadoCorreo,
                       a.tipoDocumento,
                       a.documento,
                       a.fechaNacimiento,
                       a.pais,          
                       a.id_municipio,
                       a.id_ccaa,
                       a.codigoPostal,
                       a.direccion,
                       a.lugarParticipacion,
                       a.fechaInscritpcion,
                       a.ipInscripcion,
                       a.bloqueo,
                       a.razon_bloqueo,
                       b.provincia,
                       c.nombre as municipio,
                       a.id_provincia,
                       a.id_municipio
                            FROM inscritos a,provincia b, municipios c 
                            where (a.id_provincia = b.id) and (a.id_municipio = c.id_municipio) 
                            and  a.ID NOT IN (
                        SELECT d.inscritos_ID FROM inscritos_acciones d WHERE d.status != 2)                          
                        ');
            }






//                            FROM inscritos a,provincia b, municipios c ,valida_inscritos d 
//                            where (a.id_provincia = b.id) and (a.id_municipio = c.id_municipio) and (a.ID = d.inscritos_ID) 
//                            group by d.inscritos_ID having count(*) < 2 
//                            FROM inscritos a,provincia b, municipios c ,inscritos_acciones d 
//                            where (a.id_provincia = b.id) and (a.id_municipio = c.id_municipio) and (a.ID = d.inscritos_ID) and d.status != 2  
//                            group By d.inscritos_ID');
//                         FROM inscritos a
//                            where  NOT EXISTS
//                                (SELECT  null 
//                                FROM    valida_inscritos d 
//                                WHERE   a.ID = d.inscritos_ID )

            if ($format == "csv") {
                $handle = fopen('php://output', 'w+');
                // Add the header
                fputcsv($handle, ['Nombre', 'Apellido', 'Telefono', 'Correo', 'Correo validado', 'Tipo documento', 'Documento', 'Fecha nacimiento', 'Pais', 'Provincia', 'Municipio', 'CCAA', utf8_decode('Código Postal'), utf8_decode('Dirección'), utf8_decode('Lugar participación'), utf8_decode('Fecha inscripción'), utf8_decode('IP Inscripción'), 'Bloqueado', 'Razon Bloqueo'], ';');

                while ($row = $results->fetch()) {
                    if ($row['pais'] == "ES") {

                        fputcsv($handle, array(
                            utf8_decode($row['nombre_usuario']),
                            utf8_decode($row['apellido_usuario']),
                            utf8_decode($row['telefono']),
                            utf8_decode($row['correo_usuario']),
                            utf8_decode($row['validadoCorreo']),
                            utf8_decode($row['tipoDocumento']),
                            utf8_decode($row['documento']),
                            $row['fechaNacimiento'],
                            utf8_decode($row['pais']),
                            //$row['id_provincia'],
                            utf8_decode($row['provincia']),
                            //$row['id_municipio'],
                            utf8_decode($row['municipio']),
                            utf8_decode($row['id_ccaa']),
                            utf8_decode($row['codigoPostal']),
                            utf8_decode($row['direccion']),
                            utf8_decode($row['lugarParticipacion']),
                            $row['fechaInscritpcion'],
                            utf8_decode($row['ipInscripcion']),
                            utf8_decode($row['bloqueo']),
                            utf8_decode($row['razon_bloqueo']),
                                ), ';');
                    } else {
                        fputcsv($handle, array(
                            utf8_decode($row['nombre_usuario']),
                            utf8_decode($row['apellido_usuario']),
                            utf8_decode($row['telefono']),
                            utf8_decode($row['correo_usuario']),
                            utf8_decode($row['validadoCorreo']),
                            utf8_decode($row['tipoDocumento']),
                            utf8_decode($row['documento']),
                            $row['fechaNacimiento'],
                            utf8_decode($row['pais']),
                            utf8_decode($row['provinciaOtros']),
                            utf8_decode($row['municipioOtros']),
                            utf8_decode($row['id_ccaa']),
                            utf8_decode($row['codigoPostal']),
                            utf8_decode($row['direccion']),
                            utf8_decode($row['lugarParticipacion']),
                            $row['fechaInscritpcion'],
                            utf8_decode($row['ipInscripcion']),
                            utf8_decode($row['bloqueo']),
                            utf8_decode($row['razon_bloqueo']),
                                ), ';');
                    }
                }

                fclose($handle);
            } else if ($format == "dmK") {
                $handle = fopen('php://output', 'w+');
                // Add the header
                while ($row = $results->fetch()) {
                    if ($row['pais'] == "ES") {
                        
                        fputcsv($handle, array(
                            utf8_decode($row['nombre_usuario']),
                            utf8_decode($row['apellido_usuario']),
                            utf8_decode($row['correo_usuario']),
                            utf8_decode($row['documento']),
                            $row['fechaNacimiento'],
                            utf8_decode($row['pais']),
                            $row['id_provincia'],
                            $row['id_municipio'],
                            $row['id_ccaa'],
                                ), ',');
                    } else {
                        fputcsv($handle, array(
                            utf8_decode($row['nombre_usuario']),
                            utf8_decode($row['apellido_usuario']),
                            utf8_decode($row['correo_usuario']),
                            utf8_decode($row['documento']),
                            utf8_decode($row['pais']),
                            utf8_decode($row['provinciaOtros']),
                            utf8_decode($row['municipioOtros']),
                            $row['id_ccaa'],
                                ), ',');
                    }
                }

                fclose($handle);
            }
        }
        );

        if ($format == "csv") {
            $ext = ".csv";
            $typeMine = "text/csv";
        } else if ($format == "dmK") {
            $ext = ".txt";
            $typeMine = "text/plain";
        }
        $fecha = date("hs_dmY");
        $fileName = $fecha . "_" . $name_arch . "" . $ext;

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', $typeMine . '; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment; filename=' . $fileName);
//        $response->headers->set('Content-Type', 'text/plain; charset=utf-8');
//        $response->headers->set('Content-Disposition', 'attachment; filename="export.txt"');


        return $response;
    }

}
