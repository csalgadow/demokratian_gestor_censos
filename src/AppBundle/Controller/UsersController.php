<?php

namespace AppBundle\Controller;

use BackendBundle\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * User controller.
 *
 */
class UsersController extends Controller {

    private $session;

    public function __construct() {
        $this->session = new Session();
    }

    public function loginAction(Request $request) {  //autentificamos usuarios usando los metodos de symfony de autentificación
        $authenticationUtils = $this->get("security.authentication_utils");
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();


        return $this->render('AppBundle:User:login.html.twig', array(
                    "error" => $error,
                    "last_username" => $lastUsername
        ));
    }


}
