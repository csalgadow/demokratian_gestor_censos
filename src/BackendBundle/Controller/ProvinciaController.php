<?php

namespace BackendBundle\Controller;

use BackendBundle\Entity\Provincia;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Provincium controller.
 *
 */
class ProvinciaController extends Controller
{
    /**
     * Lists all provincium entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $provincias = $em->getRepository('BackendBundle:Provincia')->findAll();

        return $this->render('BackendBundle:provincia:index.html.twig', array(
            'provincias' => $provincias,
        ));
    }

    /**
     * Finds and displays a provincium entity.
     *
     */
    public function showAction(Provincia $provincium)
    {

        return $this->render('BackendBundle:provincia:show.html.twig', array(
            'provincium' => $provincium,
        ));
    }
}
